#!/bin/sh

PATH=/usr/local/bin:/usr/bin:/bin

magick="convert"

if ! test -f your-way_sapphire_1080p.svg; then
    echo "Missing 1080p"
    exit 1
fi

if ! test -f your-way_sapphire_wide-large.svg; then
    echo "Missing wide-large"
    exit 1
fi

if ! test -f your-way_sapphire_narrow-large.svg; then
    echo "Missing narrow-large"
    exit 1
fi

mv -f -b your-way_sapphire_1080p.png \
    your-way_sapphire_1080p.png.old

mv -f -b your-way_sapphire_wide-large.png \
    your-way_sapphire_wide-large.png.old

mv -f -b your-way_sapphire_wide-small.png \
    your-way_sapphire_wide-small.png.old

mv -f -b your-way_sapphire_narrow-large.png \
    your-way_sapphire_narrow-large.png.old

mv -f -b your-way_sapphire_narrow-small.png \
    your-way_sapphire_narrow-small.png.old

# the png32: prefix is needed to retain full RGB color 
# and prevent inadvertant conversion to indexed color
# an alternative way is with the -define png:color-type='2' option

$magick your-way_sapphire_1080p.svg \
    png32:your-way_sapphire_1080p.png

$magick your-way_sapphire_wide-large.svg \
    png32:your-way_sapphire_wide-large.png

$magick your-way_sapphire_wide-large.svg \
    -resize 1280 \
    png32:your-way_sapphire_wide-small.png
 
$magick your-way_sapphire_narrow-large.svg \
    png32:your-way_sapphire_narrow-large.png

$magick your-way_sapphire_narrow-large.svg \
    -resize 1280 \
    png32:your-way_sapphire_narrow-small.png
 
ls -lht \
     your-way_sapphire_1080p.svg \
     your-way_sapphire_1080p.png \
     your-way_sapphire_wide-large.svg \
     your-way_sapphire_wide-*.png \
     your-way_sapphire_narrow-large.svg \
     your-way_sapphire_narrow-*.png

exit 0
