# THEMING A DEVUAN RELEASE

**IMPORTANT! Keep in mind that color perception is relative to adjacent colors. Only the human eye can perceive these subtle differences. Applications using a mathematical relationship most often fail at this so please don't use one.**

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [THEMING A DEVUAN RELEASE](#theming-a-devuan-release)
  - [DESKTOP](#desktop)
    - [1. wallpaper](#1-wallpaper)
    - [2. clearlooks-phenix theme](#2-clearlooks-phenix-theme)
       - [basic colors](#basic-colors)
       - [titlebar xpms and pngs](#titlebar-xpms-and-pngs)
       - [titlebar control buttons](#titlebar-control-buttons)
       - [fine-tune css](#fine-tune-css)
       - [png and svg icons](#png-and-svg-icons)
       - [openbox themerc](#openbox-themerc)
    - [3. gnome icons](#3-gnome-icons)
  - [GRUB THEME](#grub-theme)
  - [ISOLINUX SPLASH](#isolinux-splash)
  - [SLIM](#slim)
  - [LIGHTDM GREETER](#lightdm-greeter)
  - [ICONS and FAVICONS (as needed)](#icons-and-favicons-as-needed)
  - [WWW](#www)
    - [1. devuan.org](#1-devuan-org)
    - [2. other www](#2-other-www)
  - [MISCELLANEOUS](#miscellaneous)
- [FINIS!](#finis)
<!-- markdown-toc end -->

## DESKTOP

### 1. wallpaper

https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/backgrounds

Choose the release color palette using the desktop wallpaper. Try to choose colors relevant to the release name. Live with it on your desktop and tweak until it's just right. This process can take weeks or even months so be patient.

The logo and tag line may have to be a little lighter (or darker) value than the swoosh to cut through a dark (or light) background.

Wallpapers are created with 2 svgs that generate 4 different size png to accommodate various monitor configurations. Color replacement can be done in the text file and then viewed and the pngs exported in inkscape. **Note: this process is tricky and a moment of inattention can fubar it.**


Here are the dimensions to export:
  - wide-large    2560 x 1600
  - wide-small    1280 x 800
  - narrow-large  2560 x 1920
  - narrow-small  1280 x 960

### 2. clearlooks-phenix theme
https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/Clearlooks-Phenix-Deepsea.tar.gz

Work from the default clearlooks-phenix theme and diff with the files of the most recent Devuan customization to see what changes have been made for Devuan.

#### basic colors
Begin with a rough guess of what the basic color-scheme settings might be to blend with the wallpaper using "base_color", "font", "selected_bg_color", "bg_color" etc. in `Clearlooks-Phenix/gtk-2.0/gtkrc` and `Clearlooks-Phenix/gtk-3.0/gtk.css` and `Clearlooks-Phenix/gtk-3.0/settings.ini`.

#### titlebar xpms and pngs
Then move onto the titlebar images, which visually anchor the window, at `Clearlooks-Phenix/xfwm4/`. This is a tedious process of trial and error to get the color(s) just right. If it's too similar to the desktop colors, it gets “lost”.  Too much contrast, it looks out of place. It's a delicate balance. More saturation and a bit lighter value can help it pop out.

**Hint: Save some time by experimenting with gradients and mockups before messing with the xpm and png.**

Gimp handles the colorizing and exporting of xpm and png extremely well. With a little fussing in the “colorize” dialog, the Hue, Saturation and Value of the desired color can be matched perfectly and the gradients produced should be very close to, if not exactly, what you imagined.  When you think you've got it right, save the H-S-V and apply to all the 39 xpms and 31 pngs and then export.

After seeing a working xpm titlebar against the wallpaper, colors may need a few increments of tweaking.  It may take a few iterations to get it right.

#### titlebar control buttons
Then tackle the contrasting control button color.  Note that areas of the control symbols may need to be colored pixel by pixel for contrast.

#### fine-tune css
Then it's time to fine tune the css for the body of the application window. There be dragons in that gtk3.  Here are a few:

  - In the `.../gtk-3.0/gtk.css`:
    - To align the gtk2 and gtk3 selected colors, the “core-color” has to be lighter than the “selected” color due to shading rules further down.  Getting this color to match the `.../gtk-2.0/gtkrc` styling is one of the most challenging parts of the theme.
    - there are other rules down the page that will require adjustments in the trough and scrollbar etc.  Do a diff to compare previous themes with the CP in the current repo.
  - `.../gtk-2.0/gtkrc` styles the desktop folder presentation in `xfdesktop-icon-view`
  - `chrome-hacks` in `.../gtk-2.0/applications-rc` styles the titlebar for Chromium
  - In `.../gtk-3.0/gtk-widgets.css`, `cell:hover` `.cell` and `.cell:selected` will need color changes
  
#### png and svg icons
Next are the 88 png and 7 svg icons in `.../gtk-3.0/img/` which also need to be recolored. Again, gimp is very good at this.  Since the img are so small, pixels will sometimes need to be hand-colored to produce the necessary contrast.
#### openbox themerc
Last piece of the theme is the `.../openbox-3/themerc` for openbox which is a real brain-twister.  I make a template of color swatches for every color in the themerc in order of appearance.  Then recolor and switch out the hexes in the text file.  It will likely take several attempts to get it right.

### 3. gnome icons

https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/deepsea-icons.tar.gz

Again it's easier to begin with mockups to find the right default folder color.  Something close to the titlebar buttons is a good place to begin looking. The easy solution would be gnome-icons.svg but the problem is that the default shading is too dark and rather “splotchy” and the folder tab and file front are not well defined. Fixing it in the master svg was just beyond me so I did it the old-fashioned way. I suggest working off of those files to get a crisper look.  The following icons need to be recolored for all available sizes:
  - places (13)
  - actions (2)
  - status (3)
  - index.theme (file will need some text tweaks)

## GRUB THEME

https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/grub/deepsea-grub-final.tar.gz

  - Colors should mirror the wallpaper bg and swoosh colors.
  - background.png - Recolor the svg and export to png in inkscape: https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/grub/deepsea-background-grub.svg
  - devuan-logo.png - Use desktop logo color (or possibly a notch lighter). Recolor the svg and export to png in inkscape:
 https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/grub/devuan-logo-grub.svg 
  - There are 9 semi-transparent pngs which will need to be recolored to create the menu bar hilight.  It takes a bit of fussing to get the color right.
  - Change the font color in `theme.txt` but don't touch anything else.
  - Icons shouldn't need any adjustment as long as it is a dark theme.

## ISOLINUX SPLASH

https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/deepsea-background-isolinux.png

  - Uses the grub background but the logo is added to the image itself . Note that the placement is slightly different than in grub to accommodate the list of options.
  - Other colors are defined in the `sdk-menu` file.

## SLIM

https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/slim

  - There is an svg to generate the re-colored swoosh panel and export to png in inkscape:
https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/slim/slim_deepsea.svg
  - The rest of the panel background is generated by a small tiling `background.png` which will need to be recolored.
  - The colors in `slim.theme` will need to be adjusted accordingly.

## LIGHTDM GREETER

  - Needs one wide and one narrow desktop background without the logo and tagline.
  - fsmithred will also need a 650x650 round swoosh favicon for the login greeter:
https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/avatars/avatar-template.svg

## ICONS and FAVICONS (as needed)

  - One decorative rounded-square icon with border for the page title line of the devuan.org webpages:
https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/rounded-square-icon.svg   
  - Recolor the “if” icon for the www index page: https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/init-freedom
  - One 32x32 round swoosh icon to launch “Applications” in the Xfce panel.
  - One 650x650 round icon for lightdm greeter. https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/avatars/avatar-template.svg

## WWW

### 1. devuan.org

  -  css is a minefield.  Most but not all of the changes are near the end of the file:
https://git.devuan.org/devuan/www.devuan.org/src/branch/new-beta/source/ui/css/devuan.css
  - If the wallpaper colors are dark, they might need to be lightened a bit on www because the page body is light and the contrast “blackens” the already dark colors.
  - The “if” icon on the index page and square icon preceding the page titles will need to be recolored. (see above)
  - Screenshots in the Install Guides may require some processing to remove errant cursors etc. for visual consistency before replacing in the install guides.

### 2. other www

Replace colors used on devuan.org for the navbar, background, font, links and footer in the css for:

  - bugs.devuan.org (bugs.css)
  - popcon.devuan.org (buggers.css)
  - pkginfo.devuan.org (common.css) Possibly changes in the other css files.  Can't remember.

## MISCELLANEOUS

  -  Update the splashscreen svg for the minimal-live iso to the current release version:
https://git.devuan.org/devuan/documentation/src/branch/master/art/graphics/chimaera-deepsea/minimal-live-boot-splash.svg
  -  Optional: I also have always recolored the refracta wallpaper and isolinux image for fsmithred who configures all the moving parts for desktop-base and packages all the pieces.

# FINIS!
