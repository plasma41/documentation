# Devuan Artwork

Elements of the Devuan identity: graphics, sounds, etc. for use with Web 
presence, user interfaces, presentations, videos, and other promotional 
materials.

`git clone git@git.devuan.org:devuan-editors/devuan-art.git`

## Repository Organization

 - The `master` branch contains the latest stable version of everything.
 - A branch per suite, e.g., `suites/jessie`.
 - A branch per [issue](issues), e.g., `fix-123`.

### Logo

The Devuan logo source files and specifications are in `[graphics/logo][logo]`.

![Devuan Logo SVG](./graphics/logo/devuan-logo.svg)

### Graphics

Ready-to-use graphical elements (backgrounds, icons. applied logos...).

## Distribution

Devuan is a registered trademark of the Dyne.org foundation.

The above notice need to be preserved on all publications naming Devuan.

The logo is released as Creative Commons license CC-BY-SA 4.0 international (same as Wikipedia).

## Team

`devuan-art` is a project of the [Devuan Editors][editors]. If you're 
interested in contributing, please contact us on the Libera.chat IRC channel 
#devuan-art or by using the [issue tracker](../../issues).  We welcome 
talented graphical artists!


[logo]: graphics/logo
[editors]: https://git.devuan.org/groups/devuan-editors

