# Jenkins Autobuild infrastructure in Devuan

Package builds in Devuan can be triggered by opening an
[Gitea](https://git.devuan.org) issue.

A python script, [releasebot](https://git.devuan.org/devuan/releasebot), runs
every minute to scan for relevant issues. Having parsed labels and checked
permissions, it triggers the Jenkins job to build the package and deploy it to
the repository.

## Requirements to Trigger a Build

Before to start a build with the autobuilder, ensure you:

 * have a tested, buildable package source in
   https://git.devuan.org/devuan/`<package>`, with a branch named
   suites/`<codename>` where `<codename>` is the name of the suite to
   build. Maintainers have permissions to build for unstable or testing.
   Builds for released suites are restricted to administrators.
 * are a collaborator on the package or a member of the [Gitea packages
   team](https://git.devuan.org/org/devuan/teams/packages)

NB. There is no longer a requirement to set up a specific Jenkins build job for
each individual package or configure build architectures.

## Triggering a Build

Once the prerequisites are satisfied, trigger a build by creating a new issue
against the package you wish to build with the title "build". Assign the issue
to "releasebot", and add label(s) with the name of the suite(s) for which you
want to build.

When it runs, releasebot will add comments to the issue regarding its actions
and then it will close the issue.

## Jenkins Pipeline

All Devuan packages are now built through a [common
pipeline](https://jenkins.devuan.dev/job/devuan-package-builder/) (no login
needed) where you can follow the progress of your build.

The Jenkins Pipeline also sends notifications to the #devuan-ci channel on
irc.libera.chat.
