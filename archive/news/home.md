# Devuan News

Devuan News (DN) covers the current discussions on the [DNG mailing
list](https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng) and
the general news of the [Devuan Project](https://devuan.org/) to offer
a consistent starting point to the community.

## Lately in Devuan

You can [read the current issue](past-issues/volume-03/issue-060), and
[participate](edition-guidelines) in the [upcoming
issue](upcoming-issue).  Anyone willing to participate should join us
on IRC: __#devuan-news__.

## Archives

The Devuan News (DN) is sent at most once a week on the DNG mailing
list. You can access [past issues](past-issues/home) from here for
reference (DN Archives uses [Holocene
era](http://en.wikipedia.org/wiki/Holocene_calendar) for dating years,
see [why](why-he)).

- [Volume 01](past-issues/volume-01) (12014, 4 issues)
- [Volume 02](past-issues/volume-02) (12015, 9 issues)
- [Volume 03](past-issues/volume-03) (12016, 2 issues, current)

## DN Team

The Devuan News (DN) is brought to you by a volunteer team
[open to contributions](edition-guidelines).

__DN Team is looking for contributors!__

+ Founder and Editor-at-large: Noel "Envite" Torres
+ Editor: @hellekin
+ Den Mother: @golinux
+ Essayist: @dev1fanboy
+ Package Master: @Centurion_Dan
+ With help from: David Harrison, DocScrutinizer05, @MinceR, @minnesotags, @etech3.
