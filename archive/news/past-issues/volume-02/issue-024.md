# Devuan News Issue XXIV

__Volume 02, Week 19, Devuan Week 24__

Released Tuesday, 12015/05/12 [HE](why-he)

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-02/issue-024

## Editorial

You may have noticed that some time has elapsed since the last issue was published. We are sorry for that delay.

One of the reasons is the lack of free time as the newsletter is a volunteer project and there is a lot of hard work going on with the developers. 

But *Devuan Weekly News* hasn't disappeared, we're still here. And now we are officially dropping *'Weekly'* from our name. For two reasons.

The first reason is to avoid confusion with the Debian Weekly News (also DWN).  The Devuan newsletter will now be known as Devuan News (DN).

The second reason is the lack of (wo)man power. The DN will be published when it's published depending on our collective circumstances and inclinations.

A final note goes to thank all the volunteers working on the Devuan News.

*You* make this possible.

@etech3

## Lately in Devuan

### [vdev status update][3]

Jude keeps delivering the goods with `vdev`.  After having extracted
`libudev` from `systemd 219` , he shares a difficulty with
the initial idea of using `inotify(2)` to monitor devices changes, and
announces a workaround plan.

Any feedback on the above development plan is welcome, especially if a simpler, more robust approach can be found.

Thanks to all who followed [Jude's instruction to provide testing
information for vdev development][3a].  Keep'em coming!

### [Financial report, 1st trimester 2015][4]

This report from @jaromil also includes a summary of the Devuan project's development.

### [Which package generates /lib/systemd and /etc/systemd files?][5]

There was a thread started by Anto which included several interesting responses:

From jaromil - https://lists.dyne.org/lurker/message/20150505.173308.c2f664a8.en.html

From Isaac Dunham - https://lists.dyne.org/lurker/message/20150505.235121.db200a9a.en.html

### [A novice attempt to speed up Devuan development][6]

Anto posted on his Devuan installation via debootstrap.

### [Please stop vain discussion][7]

Didier commented on yet another debate over whether Devuan should allow systemd usage or forbid it completely.

@nextime responded with a statement about Devuan generic policy and systemd:

> "The official position is we will support anything that can be packaged without hijacking the whole system to be installed. Actually systemd doesn't match this requirement, so, Devuan will not support it as long it doesn't radically change. Anyway we will not intentionally obstacle anyone that eventually wants to use it outside official Devuan release."

### [I used Devuan's debootstrap and installed Devuan][8]

Several users (like Edward Bartolo and David Hare) reported their experiences with Devuan's new debootstrap.

### [Linux boot documentation][9]

Steve Litt announced his latest oeuvre which generated an interesting technical discussion of the boot process.

### Are we there yet?

There has been some chatter on and off the last few weeks (on the IRC channels) relating to the

Q: "Has there been an official release of Devuan?"

A: There is no official release as of yet, but a testable iso ([pre-alpha][pre-alpha] and a [vagrant][vagrant] version).

Q: Are there any rough estimates for the first release? is it more like 2 months or more like a year?

A: "When it's ready" :D

nextime responded "when it is ready, but at least I can assure that it isn't a year".

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Subscribe to the feed][atom-feed]
- [Read online archives][archives]

-------------------
Read you soon!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!

+ Created by Noel "@Envite" Torres
+ @hellekin (editor at large)
+ @golinux (word wrangler)
+ @lightbringer (AKA. MinceR, sentence fixaupper)
+ @DocScrutinizer05 (proofreader)
+ @etech3 (markdown master in training MMIT)

---  ---

[3]: https://lists.dyne.org/lurker/message/20150406.203624.a1b96212.en.html "vdev status update"

[3a]: https://git.devuan.org/pkgs-utopia-substitution/vdev/blob/master/how-to-test.md "How to test vdev"

[4]: https://lists.dyne.org/lurker/message/20150503.224708.236843dd.en.html "Financial report, 1st trimester 2015"

[5]: https://lists.dyne.org/lurker/message/20150505.105746.fea9a964.en.html "Which package generates /lib/systemd and /etc/systemd files?"

[6]:https://lists.dyne.org/lurker/message/20150505.065520.2303aae4.en.html "A novice attempt to speed up Devuan development"

[7]: https://lists.dyne.org/lurker/thread/20150506.194559.3168e0c6.en.html "Please stop vain discussion"

[8]: https://lists.dyne.org/lurker/message/20150503.043143.ff8d2240.en.html "I used Devuan's debootstrap and installed Devuan"

[9]: https://lists.dyne.org/lurker/message/20150504.144806.e841d24a.en.html "Linux boot documentation"

[pre-alpha]: http://mirror.debianfork.org/devuan-jessie-i386-xfce-prealpha-valentine.iso 

[vagrant]: https://lists.dyne.org/lurker/message/20150307.011135.a710525c.en.html

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG mailing list"

[atom-feed]: http://lists.devuan.org/dwn/atom.xml "Subscribe to ATOM feed"

[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"

[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"

[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan News"

[why-he]: https://git.devuan.org/devuan-editors/devuan-news/wikis/why-he