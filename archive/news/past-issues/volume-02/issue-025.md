# Devuan News Issue XXV

__Volume 02, Week 20, Devuan Week 25__

Released Tuesday, 12015/05/19 [HE](why-he)

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-02/issue-025

## Editorial

As Spring gets closer to summer and the heat starts to rise (northern hemisphere centric), so does the work at Devuan. There is tons going on behind the scenes with many people working on pre-alpha builds. 

A couple of shout outs to golinux, hope your back is better, we miss you and to Centurion_Dan on the birth of his son, congrats!

A big thank you goes to the volunteers working on the Devuan News. *You* make this possible.

@etech3

## Lately in Devuan

### [vdev status update][3]

Jude Nelson posted his latest update for vdev

Thanks to all who followed [Jude's instruction to provide testing
information for vdev development][3a].  Keep'em coming!

### [stonix, a security tool][5]
Roy Nielsen posted that  a beta release of stonix, a security tool to give non-windows operating systems a base level of security based on government guideline and industry best
practices.

https://github.com/CSD-Public/stonix 

### [eudev status][6]
jaret posted on A novice attempt to speed up Devuan development that he had pushed eudev to the git (https://git.devuan.org/jaretcantu/eudev).

>I used eudev version 1.9 since it is based on systemd 215. That is the udev/systemd version used by Jessie, so it just seemed to make a lot more sense.


### [How to bust into a broken Qemu VM][7]
Steve started How to bust into a broken Qemu VM.
>This isn't precisely about Devuan, but given the number of us testing
>Devuan in Qemu VMs, it would probably be very handy information.
>
>When you accidentally bork a Qemu VM such that it won't boot to a
>virtual terminal, how do you bust back in. I doubt System Rescue CD
>would help, unless you can boot from the "cdrom" but somehow also
>access the existing "hard disk" borked VM image.
>
>So how do you bust back into a borked VM? 

NOTE: Steve marked this solved!

### [Document on using Qemu for Linux DIY][8]
Steve Litt posted "Document on using Qemu for Linux DIY"

>Hi all,
>
>I just finished a very basic document on using Qemu for Linux DIY, which I tech-edited using Valentines Devuan:
>
>http://www.troubleshooters.com/linux/diy/qemu.htm>
>
>It's based on three shellscripts, and also contains instructions for sharing host and guest /tmp/xfer directory via sshfs. As you know, Qemu currently has no copy and paste between host and  guest, so a shared directory's a great way to transfer copied text or screenshots.
>
>The shellscripts in this doc, perhaps modified by you, might be a timesaver for some of you who are repeatedly testing Devuan.
>
>Hope you like it. 


## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Subscribe to the feed][atom-feed]
- [Read online archives][archives]

-------------------
Read you soon!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!

+ Created by Noel "@Envite" Torres
+ @hellekin (editor at large)
+ @golinux (word wrangler)
+ @lightbringer (AKA. MinceR, sentence fixaupper)
+ @DocScrutinizer05 (proofreader)
+ @etech3 (markdown master in training MMIT)

--- Links to follow ---

[3]: https://lists.dyne.org/lurker/message/20150514.032442.4939d09c.en.html "vdev status update"

[3a]: https://git.devuan.org/pkgs-utopia-substitution/vdev/blob/master/how-to-test.md "How to test vdev"

[5]: https://lists.dyne.org/lurker/message/20150513.163344.525255d9.en.html "stonix, a security tool"

[6]: https://lists.dyne.org/lurker/message/20150517.032838.0d000655.en.html "eudev status"

[7]: https://lists.dyne.org/lurker/message/20150515.233757.4fd30ed5.en.html "How to bust into a broken Qemu VM"

[8]: https://lists.dyne.org/lurker/message/20150514.231814.446caf18.en.html "Document on using Qemu for Linux DIY"

[pre-alpha]: http://mirror.debianfork.org/devuan-jessie-i386-xfce-prealpha-valentine.iso 

[vagrant]: https://lists.dyne.org/lurker/message/20150307.011135.a710525c.en.html

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG mailing list"

[atom-feed]: http://lists.devuan.org/dwn/atom.xml "Subscribe to ATOM feed"

[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"

[Financial report]: https://lists.dyne.org/lurker/message/20150503.224708.236843dd.en.html "Financial report, 1st trimester 2015"

[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"

[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan News"

[why-he]: https://git.devuan.org/devuan-editors/devuan-news/wikis/why-he
