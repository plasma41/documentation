# Devuan Weekly News Issue XVII

__Volume 02, Week 12, Devuan Week 17__

Released 03/31/12015 [HE](why-he)

https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-02/issue-017

## Editorial

### Where no toy has gone before...

We mourn the passing of actor Leonard Nimoy, most famous for his role as Star Trek's Vulcan science officer Mr. Spock. The sci-fi classic served as an inspiration for many at [NASA][1].

"Leonard Nimoy was an inspiration to multiple generations of engineers, scientists, astronauts, and other space explorers. As Mr. Spock, he made science and technology important to the story, while never failing to show, by example, that it is the people around us who matter most."

Thanks goes out to everyone who helps on the DWN

etech3

## Last Week in Devuan

### [vdev status update][3]

As in other weeks, Jude posted to the list the weekly vdev status
update. Very interesting read.

### [Puppy Linux-related thoughts...][4]

Isaac Dunham started a post that there may be some guidelines from Puppy Linux for building a small, lightweight, fast, and relatively easy to use system, even for total Linux newbies. Includes a link to distrowatch.com on an antiX Jessie beta without systemd.

Apollia had a related post about Gobo Linux, Puppy [Linux.][5]  

### [Why daemontools is so cool][7]

Steve Litt started this post listing some features he likes in daemontools. A lot of followup posts.

### [Any plans to provide xinit without the systemd hacks?][8]

### [Is libudev-compat ready for testing][9]

shraptor shraptor (or Scooby) saw Jude had committed some code and asked if libudev-compat is ready for testing. He further asked if there was there any testing done on vagrant image? 

Jude replied that it's not quite ready - it's not yet ABI-compatible with libudev (but it is API-compatible).

Jaromil commented that sounds great. libudev-compat is the last thing standing in the way of removing systemd packages completely AFAIK 

### [Another reason why I am considering Devuan][11]

It was pointed out that Debian had a systemd-related bug regarding DNS with follow ups relating to name server set up.

[11]:https://lists.dyne.org/lurker/message/20150329.130743.e7a627ec.en.html
[9]:https://lists.dyne.org/lurker/message/20150328.191653.370f97d3.en.html
[8]:https://lists.dyne.org/lurker/message/20150328.152643.98b7ea1f.en.html
[7]:https://lists.dyne.org/lurker/message/20150327.233205.623f3aa0.en.html
[6]:https://lists.dyne.org/lurker/message/20150327.233205.623f3aa0.en.html
[5]:https://lists.dyne.org/lurker/message/20150324.190730.1d70bafe.en.html
[4]:https://lists.dyne.org/lurker/message/20150323.055207.4da7fc30.en.html
[1]:http://www.nasa.gov/content/nasa-remembers-leonard-nimoy/
[2]:https://git.devuan.org/Envite/devuan-weekly-news/issues/2
[3]:https://lists.dyne.org/lurker/message/20150324.093704.b72a5258.en.html






## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Subscribe to the feed][atom-feed]
- [Read online archives][archives]

-------------------
Read you next week!

Devuan Weekly News is made by your peers: you're [welcome to contribute][wiki]!

...

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG mailing list"
[atom-feed]: http://lists.devuan.org/dwn/atom.xml "Subscribe to ATOM feed"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
[why-he]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/Why-HE "Why Holocene Era?"
