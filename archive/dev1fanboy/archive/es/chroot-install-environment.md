Esta documentación es experimental, necesita mejoras en las intrucciones de chroot.

# Crear un entorno de instalación para chroot

El propósito de este documento es mostrar cómo crear una instalación de Devuan separada del sistema en ejecución. Esto
puede ser útil para implementar virtualización, o instalar ciertos paquetes sin alterar el sistema en ejecución, lo
cual es útil para propósitos de testeo.

A modo de ejemplo utilizaremos la versión Ceres (inestable) al momento de instalar el entorno para chroot. En caso de
querer utilizar una versión diferente, reemplazar `ceres` por otro nombre de *release*.

## Crear el entorno de instalación de chroot

Instalar el paquete debootstrap necesario para la instalación.

`root@devuan:~# apt-get install debootstrap`

Es necesario definir una ubicación para el chroot, por lo tanto se debe crear un directorio a tal fin.

`root@devuan:~# mkdir /mnt/ceres`

Completar el proceso de instalación del chroot utilizando debootstrap.

`root@devuan:~# debootstrap ceres http://pkgmaster.devuan.org/merged /mnt/ceres`

## Ejecutar chroot hacia la instalación

Cuando se utiliza un entorno de chroot es necesario asegurarse de que `dev`, `proc` y `sys` estén disponibles desde
allí.

`root@devuan:~# mount -o bind /dev /mnt/ceres/dev`

`root@devuan:~# mount -o bind /sys /mnt/ceres/sys`

`root@devuan:~# mount -t proc proc /mnt/ceres/proc`

Ejecutar chroot hacia el entorno para comenzar a utilizarlo.

`root@devuan:~# chroot /mnt/ceres /bin/bash`

## Salir del entorno de chroot
Al finalizar es necesario salir del entorno chroot de manera ordenada.

`root@chroot:~# exit`

`root@devuan:~# umount /mnt/ceres/dev`

`root@devuan:~# umount /mnt/ceres/sys`

`root@devuan:~# umount /mnt/ceres/proc`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

