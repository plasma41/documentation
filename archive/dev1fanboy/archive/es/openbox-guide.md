# Guía de Openbox
Esta es una introducción a openbox para quienes nunca lo hayan utilizado antes. Provee una configuración que ofrece muchas de las características que los entornos de escritorio tienen, mientras que se mantiene tan mínima como sea posible. 

## Instalar los paquetes
Comenzaremos instalando los paquetes mínimos necesarios para utilizar y configurar openbox.

`root@devuan:~# apt-get install openbox obconf obmenu`

También es útil instalar el paquete menu, pues provee los ítems de menú para el *software* que lo soporta.

`root@devuan:~# apt-get install menu`

A continuación se instala el gestor de ventanas compton.

`root@devuan:~# apt-get install compton`

El programa idesk sirve para gestionar los íconos y fondo de escritorio.

`root@devuan:~# apt-get install idesk`

Para contar con una barra de tareas liviana se utiliza tint2.

`root@devuan:~# apt-get install tint2`

El gestor de archivos Xfe es capaz de montar volúmenes sin un *backend* de auto-montaje, contando con que se haya configurado correctamente en el archivo fstab.

`root@devuan:~# apt-get install xfe`

Por defecto, openbox utiliza scrot para tomar capturas de pantalla cuando se presiona la tecla `Impr Pant Pet Sis` (`Print Screen` en teclados en inglés). Sólo es necesario instalar scrot para que funcione.

`root@devuan:~# apt-get install scrot`

En la sección de configuración de idesk utilizaremos el fondo por defecto de Devuan, conocido como purpy, el cual es provisto por el paquete desktop-base.

`root@devuan:~# apt-get install desktop-base`

## Configuración de Openbox
Cuando iniciamos openbox, queremos asegurarnos de que el gestor de ventanas, el gestor de escritorio y la barra de tareas estén disponibles. Para ello es necesario que el directorio openbox exista dentro de ~/.config y luego crear un archivo de inicio para que openbox lo lea. 

`user@devuan:~$ mkdir -p ~/.config/openbox`

`user@devuan:~$ editor ~/.config/openbox/autostart`

En el archivo de inicio de openbox agregar lo siguiente (tener en cuenta que cada línea finaliza con `&`, excepto la última).

~~~
compton &
idesk &
tint2
~~~

## Configuración de idesk
Para poder utilizar idesk es necesario crear un directorio para almacenar íconos de escritorio. De lo contrario no iniciará.

`user@devuan:~$ mkdir ~/.idesktop`

Iniciar idesk para que genere la configuración por defecto. Luego se debe matar el proceso utilizando la combinación de teclas `Ctrl+C`.

`user@devuan:~$ idesk`

Modificar el archivo de configuración y establecer el fondo de escritorio.

`user@devuan:~$ editor ~/.ideskrc`

Para nuestros propósitos utilizaremos el fondo purpy, tal como fue mencionado previamente.

~~~
Background.File: /usr/share/images/desktop-base/your-way_purpy-wide-large.png
~~~

A continuación definiremos un ícono para Xfe utilizando el enlace de escritorio por defecto como plantilla.

`user@devuan:~$ cp ~/.idesktop/default.lnk ~/.idesktop/xfe.lnk`

`user@devuan:~$ editor ~/.idesktop/xfe.lnk`

Modificar el nuevo enlace para que quede como se muestra a continuación. Cambiar la posición vertical del ícono (`Y:`) para que no se superponga con el original.

~~~
table Icon
  Caption: Xfe
  Command: /usr/bin/xfe
  Icon: /usr/share/pixmaps/xfe.xpm
  Width: 48
  Height: 48
  X: 30
  Y: 120
end
~~~

## Uso del gestor de sesiones de openbox
Si se utiliza un gestor de pantalla es posible iniciar sesión en el escritorio de openbox seleccionando openbox-session como gestor de sesiones. Sino, es posible utilizar el archivo de configuración xinitrc del usuario e invocar el script startx desde consola una vez ingresado con éxito.

`user@devuan:~$ echo "exec openbox-session" > ~/.xinitrc`

`user@devuan:~$ startx`

Si se prefiere es posible cambiar el gestor de sesiones por defecto para que sea siempre openbox-session para todos los usuarios del sistema.

`root@devuan:~# update-alternatives --config x-session-manager`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>
