# Guía de instalación de Jessie

Esta es una guía fácil para instalar Devuan Jessie desde una imagen de CD/DVD para el hardware soportado. Notar que
se trata de una guía y no un manual, con lo cual no abarca todos los aspectos del instalador. El consejo general que
acompaña a esta guía es que se debe generar una copia de respaldo de todos los datos antes de comenzar el proceso de
instalación.

## Contenido
[Prerrequisitos](#prerequisites)  
[Arquitecturas soportadas](#supported-architectures)  
[Imágenes de instalación](#installation-images)  
[Instalación de Devuan Jessie](#installing-devuan-jessie)

## Prerrequisitos

* Los usuarios de sistemas operativos no *nix (Unix, Linux, etc.) deben saber cómo grabar una imagen en un medio de
almacenamiento removible como un CD/DVD-ROM o un dispositivo USB, cómo cambiar el orden de inicio en el BIOS y cómo
instalar un sistema operativo.
* Otros usuarios deberán saber lo mismo para sistemas *nix y estar algo familiarizados con el uso de la terminal.

## Arquitecturas soportadas

* amd64
* i386

## Imágenes de instalación

Actualmente estas son las formas de obtener y descargar las imágenes de instalación de Jessie.

* Directamente desde el [archivo de Devuan](https://files.devuan.org)
* Desde cualquiera de los *mirrors* listados en [devuan.org](https://www.devuan.org)
* Vía un archivo [torrent](https://files.devuan.org/devuan_jessie.torrent) que contiene todas las imágenes disponibles

Si se cuenta con acceso a Internet se sugiere utilizar una única imagen de DVD completo para la instalación, de lo
contrario probablemente se desee utilizar el conjunto completo.

Por favor utilizar los *mirrors* o torrents cuando sea posible.

Quienes no estén familiarizados con la línea de comandos pueden [saltar a la instalación](#installing-devuan-jessie).

### Verificar la integridad de las imágenes

Antes de grabar la imagen a un medio removible, es mejor verificar la integridad para asegurarse de que no esté
corrompida. Esto evita problemas que puedan ocurrir luego durante la instalación.

Descargar los [SHA256SUMS](https://files.devuan.org/devuan_jessie/installer-iso/SHA256SUMS) desde el [archivo de
Devuan](https://files.devuan.org) y verificar la integridad de la imagen.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Verificar las imágenes

Las imágenes de instalación distribuidas por Devuan están firmadas a fin de que pueda garantizarse que provienen de
Devuan. Verificar las imágenes permite saber que no han sido alteradas por terceros antes de haberlas descargado.

Obtener las [claves de los desarrolladores de Devuan](https://files.devuan.org/devuan-devs.gpg) e importarlas en
nuestro *keychain*.

`user@hostname:~$ gpg --import devuan-devs.gpg`

Utilizar los [SHA256SUMS](https://files.devuan.org/devuan_jessie/installer-iso/SHA256SUMS.asc) firmados para verificar
la imagen.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

### Grabar la imagen a un CD/DVD o dispositivo USB

Las imágenes pueden ser grabadas en un CD o DVD utilizando wodim.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

Todas las imágenes ISO son híbridas y pueden ser escritas a un dispositivo USB utilizando dd.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Instalar Devuan Jessie

Para la instalación se utiliza un instalador no gráfico. Esto es generalmente más rápido que un instalador gráfico y
consume menos recursos. Este procedimiento es adecuado tanto para instalar en una máquina virtual como para instalar
directamente en hardware. Mientras que puede resultar algo intimidante para nuevos usuarios, no hay nada que temer
dado que el proceso es completamente guiado.

&nbsp;

**1) Iniciar desde el CD/DVD o dispositivo USB y seleccionar la opción `Install`. El resto de las opciones están fuera
del alcance de esta guía.**

![Menú de instalación de Jessie](../img/firstboot.png)

&nbsp;

**2) En los siguientes pasos se selecciona el idioma, ubicación y teclado.**

![Distribución de teclado](../img/keyboard.png)

&nbsp;

**3) El instalador configura la red automáticamente, pero para el caso de redes inalámbricas el usuario debe indicar
el SSID y la contraseña. Luego se debe elegir un nombre de *host* para el nuevo sistema. No es posible utilizar
espacios o caracteres especiales.**

![Hostname](../img/hostname.png)

&nbsp;

**4) También se deberá proveer un nombre de dominio. Si no se requiere o se desconoce, simplemente dejarlo en blanco.**

![Nombre de dominio](../img/domainname.png)

&nbsp;

**5) Se recomienda configurar una clave para el superusuario root. Por razones de seguridad, se debe elegir una
contraseña fuerte. Es necesario repetir el ingreso de la contraseña para asegurarse de que sea correcta.**

![Contraseña de root](../img/rootpassword.png)

&nbsp;

**6) Luego se debe configurar una cuenta de usuario no privilegiado, que será utilizado normalmente en la mayoría de
los casos. No hay necesidad de ingresar el nombre completo a menos que haya una razón para ello, por lo cual es
posible proceder e indicar un nombre de usuario.**

![Nombre de usuario](../img/username.png)

&nbsp;

**7) Al igual que para root, será necesario especificar una contraseña e ingresarla dos veces.**

![Contraseña del usuario](../img/userpassword.png)

&nbsp;

**8) El instalador configurará ahora el reloj utilizando [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol).
Ingresar la información de la zona horaria y continuar la instalación.**

![Configuración del reloj](../img/configureclock.png)

&nbsp;

**9) Antes de poder instalar Devuan el disco debe ser particionado. Si está disponible como opción,
elegir el mayor espacio continuo disponible es lo recomendado. En tal caso las particiones existentes no
necesitan ser alteradas. De lo contrario es posible seleccionar el disco completo si no contiene datos que se
deseen mantener. Si se requiere encriptación completa del disco ver este [enlace](full-disk-encryption.md).**

![Utilizar el espacio más grande disponible](../img/partitiondisks1.png)

&nbsp;

**10) Seleccionar todos los archivos en una única partición es lo recomendado para usuarios nuevos. El particionado
manual está fuera del alcance de esta guía.**

![Todos los archivos en una única partición](../img/partitiondisks2.png)

&nbsp;

**11) A continuación se procede a escribir las particiones a disco y formatear los sistemas de archivos en ellas.
Si se está de acuerdo con los cambios, continuar y escribir el disco. Es necesario confirmar esto antes de que los
cambios se apliquen.**

![Aplicar los cambios en disco](../img/partitiondisks3.png)

&nbsp;

**12) Ahora se instala el sistema base. Dependiendo del hardware este proceso puede llevar cierto tiempo.**

![Instalación del sistema base](../img/installing.png)

&nbsp;

**13) El instalador permite seleccionar un *mirror*. Si se posee acceso a la red esto es lo recomendado debido a que se
instalan las últimas versiones disponibles de los paquetes. En tal caso, seleccionar tu país de la lista.**

![Seleccionar un mirror](../img/networkmirror1.png)

&nbsp;

**14) Cualquiera de los *mirrors* seleccionados está bien. En el caso de que exista un mirror cercano a la ubicación
actual, será redireccionado al mismo de manera automática al momento de instalar los paquetes.**

![Uso de un mirror](../img/networkmirror3.png)

&nbsp;

**15) Devuan posee un concurso de popularidad para recolectar información acerca de los paquetes más utilizados. Esto
sólo tiene como propósito mejorar la calidad de la distribución y sólo recolecta estadísticas de los paquetes
instalados por el instalador y luego el usuario.**

![Concurso de popularidad](../img/popcon.png)

&nbsp;

**16) La instalación permite elegir paquetes preseleccionados. Generalmente la selección de paquetes por defecto está
bien. En caso de duda mantener la selección por defecto.**

![Selección del software](../img/selectsoftware.png)

&nbsp;

**17) El instalador entonces procede con la instalación de los paquetes seleccionados. Esto puede demorar un momento.**

![Instalando paquetes](../img/retrieving.png)

&nbsp;

**18) En este punto todo el software necesario está instalado, el gestor de inicio GRUB será instalado. Esto permite
que el sistema operativo inicie luego de la instalación. En caso de que el instalador solicite instalarlo en el sector
MBR, reponder que sí. Algunas instalaciones no requieren configuración y la instalación finaliza aquí.**

![Instalando el gestor de inicio GRUB](../img/grub-mbr.png)

&nbsp;

**19) Es importante seleccionar la ubicación correcta para el gestor de inicio. No debe ser instalado en una
partición, sino en el sector MBR del disco rígido. En este caso `/dev/sda` es el único disco rígido presente, con lo
cual se instala en el mismo.**

![Seleccionar la ubicación del gestor de inicio](../img/grubtohdd.png)

&nbsp;

**20) La instalación ha terminado, remover el medio de instalación para continuar e iniciar en el entorno gráfico
de Devuan.**

![Finalizada la instalación](../img/finish.png)

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

