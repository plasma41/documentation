# Openbox guide
This is an introduction to openbox for those who have never used it before. It provides a setup that offers many of the features desktop environments have whilst being as minimal as possible.

## Installing the packages
We will start by installing the minimum packages necessary to use and configure openbox.

`root@devuan:~# apt-get install openbox obconf obmenu`

It is also helpful to install the menu package as it provides menu items for software that supports it.

`root@devuan:~# apt-get install menu`

We should now install the compton composite manager.

`root@devuan:~# apt-get install compton`

The idesk program can be used to manage desktop icons and background images.

`root@devuan:~# apt-get install idesk`

For a task bar we will use the lightweight tint2.

`root@devuan:~# apt-get install tint2`

The Xfe file manager is able to mount volumes without an auto-mounter backend, provided you have set this up in fstab.

`root@devuan:~# apt-get install xfe`

By default openbox uses scrot to take screenshots when the `Print Screen` key on your keyboard is pressed. Scrot only has to be installed for it to work.

`root@devuan:~# apt-get install scrot`

In the section on configuring idesk we will be using the default Devuan background known as purpy, which is provided by the desktop-base package.

`root@devuan:~# apt-get install desktop-base`

## Configure Openbox
When we start openbox we want to make sure that our composite manager, desktop manager, and taskbar is available. We will need to make sure the openbox directory exists in ~/.config and create an autostart file for openbox to read.

`user@devuan:~$ mkdir -p ~/.config/openbox`

`user@devuan:~$ editor ~/.config/openbox/autostart`

In the autostart file add the following, noting that each line ends with `&` except the last line.

~~~
compton &
idesk &
tint2
~~~

## Configure idesk
In order to use idesk we need to create the directory it uses to store desktop icons or it will not start.

`user@devuan:~$ mkdir ~/.idesktop`

Start idesk so that it generates the default configuration. You should kill the process using `ctrl + c` afterwards.

`user@devuan:~$ idesk`

Modify the config file so we can set the background.

`user@devuan:~$ editor ~/.ideskrc`

For our purposes we will use the purpy background as mentioned.

~~~
Background.File: /usr/share/images/desktop-base/your-way_purpy-wide-large.png
~~~

Next we will setup an icon for Xfe using the default desktop link as a template.

`user@devuan:~$ cp ~/.idesktop/default.lnk ~/.idesktop/xfe.lnk`

`user@devuan:~$ editor ~/.idesktop/xfe.lnk`

Modify the new desktop link look like the following. Remember to change the vertical position (`Y:`) of the icon so it does not overlap the default.

~~~
table Icon
  Caption: Xfe
  Command: /usr/bin/xfe
  Icon: /usr/share/pixmaps/xfe.xpm
  Width: 48
  Height: 48
  X: 30
  Y: 120
end
~~~

## Using the openbox session manager
If you use a display manager you may now login to your openbox desktop by choosing openbox-session as the session manager. If not you can use your user xinitrc configuration file and invoke the startx script after you login at the console.

`user@devuan:~$ echo "exec openbox-session" > ~/.xinitrc`

`user@devuan:~$ startx`

If you prefer you can change the default session manager to always be openbox-session for all users.

`root@devuan:~# update-alternatives --config x-session-manager`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
