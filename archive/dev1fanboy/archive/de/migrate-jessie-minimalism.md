# Umstellung auf Devuan Jessie mit Minimalismus
Diese Anleitung führt dich durch die nötigen Schritte zur Umstellung auf Devuan, die Konfiguration von apt für Minimalismus, entfernen von D-Bus aus dem System und Verwaltung des Netzwerks.

## Umstellung auf Devuan
Um die Umstellung zu beginnen, editiere zuerst die sources.list, sodaß wir das Paketverzeichnis mit apt-get aktualisieren können.

`root@debian:~# editor /etc/apt/sources.list`

Kommentiere in deiner sources.list alle Zeilen aus und ergänze die folgenden.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Aktualisiere das Paketverzeichnis, sodaß wir die Pakete aus dem Devuan Archiv holen können.

`root@debian:~# apt-get update`

Vor der Installation neuer Pakete müssen wir den Devuan Schlüsselbund installieren und das Paketverzeichnis erneut aktualisieren, sodaß die Echtheit der Pakete überprüft werden kann.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Nun, da der Schlüsselbund installiert ist, solltest du das Paketverzeichnis erneut aktualisieren, sodaß von jetz an die Echtheit der Pakete überprüft werden kann.

`root@debian:~# apt-get update`

Führe die Update-Prozedur zu Ende durch.

`root@debian:~# apt-get dist-upgrade`

## Konfiguriere Minimalismus
Dank eines Tipps von [TheFlash] kannst du dein System auf elegante Weise verschlanken. Wir werden apt so konfigurieren, dass empfohlene Pakete als unwichtig behandelt werden.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Ergänze die folgenden Zeilen:

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

Obwohl die meisten empfohlenen Pakete eher unwichtig sind, gibt es doch ein paar Pakete die du vor der Entfernung schützen solltest.

Für die Sicherheit deiner Browser und anderer Anwendungen, sollten wir sicher stellen, dass SSL Zertifikate immer zur Verfügung stehen, indem wir das Paket ca-certificates installieren. Überspringe diesen Schritt nur wenn du sicher bist was du tust.

`root@devuan:~# apt-get install ca-certificates`

Das SSL Zertifikate Paket ist nun als manuell installiert gekennzeichnet, anstatt als abhängiges oder empfohlenes Paket. Falls der nächste Schritt Pakete anzeigt die du behalten möchtest, kannst du mit diesen das gleiche tun, bevor du einer Entfernung zustimmst.

`root@devuan:~# apt-get autoremove --purge`

## Devuan ohne D-Bus
Die Entfernung von dbus ist komplizierter und erfordert einige Kompromisse.

### Mounten von Laufwerken als Benutzer
Eine Alternative zum D-Bus abhängigen Automount ist, die Mountpunkte selber zu konfigurieren, und einen Dateimanager zu installieren der Laufwerke ohne D-Bus mounten kann.

Da wir die fstab editieren werden solltest du zuvor eine Sicherung davon machen.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Jetzt kannst du die fstab editieren.

`root@devuan:~# editor /etc/fstab`

Hänge folgende Zeile an deine fstab an und ersetze die korrekten Device-Knoten für deine USB-Laufwerke, falls sie davon abweichen. Versichere dich die Option `user` anzugeben, um Benutzern ohne Root-Rechte das mounten des Laufwerks zu erlauben.

~~~
/dev/sdb1        /media/usb0    auto    user,noauto    0 0
~~~

Du kannst das USB-Laufwerk anstecken und das `lsblk` Dienstprogramm benutzen um die korrekten Device-Knoten zu bestimmen.

Erzeuge den Mountpunkt an dem USB-Laufwerke eingehängt werden können.

`root@devuan:~# mkdir /media/usb0`

Stecke das USB-Laufwerk an und teste dein Werk als regulärer Benutzer.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### D-Bus unabhängige Software installieren
Die meisten Desktopumgebungen benötigen dbus, deshalb sollte stattdessen ein Fenstermanager gewählt werden. Wir werden fluxbox verwenden, da er intuitiv benutzbar ist, leichtgewichtig und leicht erweiterbar ist.

`root@devuan:~# apt-get install fluxbox menu fbpager feh`

Benutze update-alternatives um fluxbox als voreingestellten Fenstemanager festzulegen.

`root@devuan:~# update-alternatives --config x-window-manager`

Wenn du einen Display Manager hinzufügen möchtest kannst du WDM benutzen.

`root@devuan:~# apt-get install wdm`

Alternativ kann das startx script aufgerufen werden, nachdem du dich an der Konsole angemeldet hast.

`user@devuan:~$ startx`

Als Dateimanager der entfernbare Laufwerke ohne einen Auto-Mounter mounten kann, kannst du xfe nutzen.

`root@devuan:~# apt-get install xfe`

Eine gute Wahl für einen Browser ist firefox-esr, da er nicht direkt auf dbus angewiesen ist.

`root@devuan:~# apt-get install firefox-esr`

### Netzwerk konfigurieren
Anstatt einen dbus abhängigen Netzwerkmanager zu benutzen, werden wir das Netzwerk für die Nutzung mehrerer Interfaces manuell konfigurieren.

`root@devuan:~# editor /etc/network/interfaces`

Hier ist eine Konfiguration für mehrere Funknetzwerke auf dem gleichen Interface. Durch hinzufügen eines Eintrags für ein Netzwerk welches nur zeitweise genutzt wird, kannst du die voreingestellte Netzwerkkonfiguration bei Bedarf übersteuern. Für weitere Information siehe [the debian reference](https://www.debian.org/doc/manuals/debian-reference/ch05.en.html#_the_manually_switchable_network_configuration) über umschaltbare Netzwerkkonfigurationen, auf die ich mich im Funknetzwerkabschnitt beziehe.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase

iface work inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

So kannst du z.B. das Netzwerk wechseln, indem du als root `ifdown wlan0` und `ifup wlan0=work` nutzt.

Kabelgebundene Netzwerk Konfigurationen sind ein ganzes Stück einfacher.

~~~
# Automatic network configuration, brought up only when a link is detected.
allow-hotplug eth0
iface eth0 inet dhcp

# Static network configuration, always brought up on boot.
auto eth1
iface eth1 inet static
        address 192.168.1.5
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

Für weitere Informationen über Interfaces siehe `man 5 interfaces`.

## Fertig stellen
Ein einmaliger Neustart ist nötig um systemd zu entfernen, da es als pid1 läuft.

`root@devuan~# reboot`

Jetzt kannst du gefahrlos systemd und dbus entfernen.

`root@devuan:~# apt-get purge systemd systemd-shim libsystemd0 dbus`

Der Gnome Desktop ist momentan ohne systemd nicht nutzbar. Mit einem regex können wir alles Gnome Pakete erfassen, sodaß wir sie nicht einzeln entfernen müssen.

`root@devuan~# apt-get purge .*gnome.*`

Jetzt kann es notwendig sein, die Xorg Pakete vor einer Entfernung zu schützen.

`root@devuan~# apt-get install xorg`

Jetzt wollt ihr vielleicht eure empfohlenen und verwaisten Pakete los werden.

`root@devuan:~# apt-get autoremove --purge`

Nun ist ein guter Zeitpunkt, um übrig gebliebene alte Pakete aus der Debian Installation zu entfernen.

`root@devuan:~# apt-get autoclean`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
