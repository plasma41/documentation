Questa guida è valida unicamente per le migrazioni dalla versione Stretch di Debian. Nelle migrazioni verso Beowulf le istruzioni sono specifiche per la versione Debian dalla quale si proviene e vanno seguite scrupolosamente.

# Migrazione da Debian Stretch a Beowulf

Per cominciare è necessario [configurare la rete](network-configuration.md), altrimenti perderete l'accesso durante la migrazione.

Per le migrazioni in provenienza da Stretch bisogna installare sysvinit-core prima di tutto.

`root@debian:~# apt-get install sysvinit-core`

Per evitare blocchi al riavvio bisogna rimuovere libpam-systemd. Questo potrebbe rimuovere anche l'ambiente grafico, che reinstallerete dopo.

`root@debian:~# apt-get purge libpam-systemd`

Modificate il file sources.list di modo che punti ai repository Beowulf.

`root@debian:~# editor /etc/apt/sources.list`

Modificate sources.list come descritto qui di seguito. Commentate tutto il resto.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Aggiornate la lista dei pacchetti dai repository Beowulf.

`root@debian:~# apt-get update`

Installate il "portachiavi" Devuan così da autenticare i pacchetti seguenti.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Aggiornate nuovamente l'indice dei pacchetti così che siano autenticati con il "portachiavi".

`root@debian:~# apt-get update`

Per evitare incongruenze va aggiornato il pacchetto libtinfo.

`root@debian:~# apt-get install libtinfo6`

Aggiornate i pacchetti alla versione più recente. Notate bene che a questo punto la migrazione non è completa.

`root@debian:~# apt-get upgrade`

È richiesto un riavvio per attribuire a sysvinit il pid1.

`root@debian:~# reboot`

Infine, prima della migrazione va installato eudev.

`root@debian:~# apt-get install eudev`

Adesso la migrazione vera e propria. In provenienza da Stretch si raccomanda di lanciare un full-upgrade.

`root@debian:~# apt-get full-upgrade`

Se l'aggiornamento si interrompe dovrete risolvere il problema relativo ai pacchetti discordanti e rilanciare il full-upgrade.

`root@debian:~# apt-get -f install`  
`root@debian:~# apt-get full-upgrade`

Se non avete un ambiente desktop a questo punto dovreste installarne uno. Quello di default in Devuan è XFCE.

`root@devuan:~# apt-get install task-xfce-desktop`

Oppure potete installare Gnome qualora preferiste continuare ad usarlo.

`root@devuan:~# apt-get install task-gnome-desktop`

Si può rimuovere systemd senza problemi.

`root@devuan:~# apt-get purge systemd`

Rimuovete tutti i pacchetti orfani lasciati dalla procedura di migrazione e tutti i pacchetti inutilizzabili dell'installazione Debian precedente.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
