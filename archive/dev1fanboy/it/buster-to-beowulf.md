Questa guida è valida unicamente per le migrazioni dalla versione Buster di Debian. Nelle migrazioni verso Beowulf le istruzioni sono specifiche per la versione Debian dalla quale si proviene e vanno seguite scrupolosamente.

# Migrazione da Debian Buster verso Beowulf

Nella migrazione verso Beowulf perderete il pacchetto network-manager. La soluzione consiste nell'uso del gestore di rete wicd, ma se preferite la [configurazione della rete](network-configuration.md) manuale, dovrete farla ora prima di poter proseguire.

Prima di tutto modificate il file sources.list di modo che punti ai repository Beowulf.

`root@debian:~# editor /etc/apt/sources.list`

Modificate sources.list come descritto qui di seguito. Commentate tutto il resto.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Aggiornate la lista dei pacchetti dai repository Beowulf. Modifiche recenti ad APT non permettono più di farlo, ma il divieto può essere aggirato.

`root@debian:~# apt-get update --allow-insecure-repositories`

Il "portachiavi" Devuan va installato ora così da autenticare i pacchetti seguenti.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Aggiornate nuovamente l'indice dei pacchetti così che siano autenticati con il "portachiavi".

`root@debian:~# apt-get update`

Se volete usare il gestore di rete wicd questo va installato ora o la procedura fallirà.

`root@debian:~# apt-get install wicd-gtk`

Aggiornate i pacchetti alla versione più recente. Notate bene che a questo punto la migrazione non è completa.

`root@debian:~# apt-get upgrade`

Una volta fatto bisogna installare eudev. Notate che se usate Gnome questo sarà rimosso dal comando seguente, ma potrà comunque essere installato nuovamente a migrazione ultimata.

`root@debian:~# apt-get install eudev`

Questo comando causa incongruenze nell'archivio dei pacchetti, ma queste verranno ricomposte durante il processo di migrazione.

`root@debian:~# apt-get -f install`

È richiesto un riavvio per attribuire a sysvinit il pid1.

`root@debian:~# reboot`

Adesso la migrazione vera e propria.

`root@debian:~# apt-get dist-upgrade`

Avete Devuan ora, quindi systemd e i pacchetti affini non sono più necessari.

`root@devuan:~# apt-get purge systemd libnss-systemd`

Se non avete un ambiente desktop a questo punto dovreste installarne uno. Quello di default in Devuan è XFCE.

`root@devuan:~# apt-get install task-xfce-desktop`

Oppure potete installare Gnome qualora preferiste continuare ad usarlo.

`root@devuan:~# apt-get install task-gnome-desktop`

Rimuovete tutti i pacchetti "orfani" lasciati dalla procedura di migrazione e tutti i pacchetti inutilizzabili dell'installazione Debian precedente.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
