# Software scevro da D-Bus in Devuan
Questa lista include software che in Devuan è completamente libero dalla zavorra di D-Bus.

## Interfacce grafiche
Con i suggerimenti della comunità Devuan.

* fluxbox
* blackbox
* openbox
* fvwm
* fvwm-crystal
* icewm
* metacity
* evilwm
* windowmaker
* mutter
* sawfish
* icewm
* dwm
* i3
* wmaker

## Gestori dei file
* xfe
* pcmanfm
* rox-filer
* mc
* gentoo
* fdclone
* s3dfm
* tkdesk
* ytree
* vifm

## Gestori grafici del login
* nodm
* ldm
* xdm
* wdm

## Browser web
* midori
* surf
* dillo
* links2
* links
* elinks
* lynx
* edbrowse
* w3m

## Lettori musicali
* deadbeef
* cmus
* cdcd
* lxmusic
* randomplay

## Lettori multimediali
* totem
* xine-ui
* mplayer2
* mpv
* melt

## Client IRC
* weechat
* irssi
* ekg2-ui-gtk
* scrollz
* loqui

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
