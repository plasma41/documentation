Questa guida contiene le istruzioni per la configurazione della rete durante la migrazione da Debian verso Devuan.

# Configurazione della rete
Per la migrazione si può scegliere tra l'uso di wicd e la configurazione manuale.

Quest'ultima ha i vantaggi di non dipendere da D-Bus e di non richedere un gestore delle reti. Per le migrazioni da remoto dovreste sempre optare per la configurazione manuale al fine di conservare l'accesso al sistema remoto nel corso della procedura.

L'uso di un gestore di rete ha i vantaggi della semplicità e dell'interfaccia grafica. Questo metodo dovrebbe essere usato **solo** in caso di guide alla migrazione che non includano l'installazione di wicd.

## Utilizzo del gestore di rete wicd
Inanzitutto installate il pacchetto wicd. Il pacchetto usato qui di seguito fornisce sia l'interfaccia gtk che quella curses per terminale.

`root@debian:~# apt-get install wicd`

Il demone di gestione di rete dovrebbe essere fermato e rimosso dalla sequenza di avvio.

`root@debian:~# service network-manager stop`  
`root@debian:~# update-rc.d -f network-manager remove`

Riavviate il demone wicd per attivare la gestione di rete.

`root@debian:~# service wicd restart`

Gli utenti di connessioni senza fili dovrebbero configurare le reti per mezzo del client gtk wicd incluso nell'ambiente desktop prescelto.

## Configurazione manuale delle connessioni cablate
Le nuove versioni di Debian presentano una denominazione delle interfacce di rete controintuitiva, come `ens3`, quindi per sicurezza includete entrambe le denominazioni nella configurazione.

Notate bene che a migrazione avvenuta la denominazione Debian non verrà rilevata e quindi non verrà più presa in considerazione.

Potrete gestire le connessioni cablate configurandole nel file `interfaces`.

`root@debian:~# editor /etc/network/interfaces`

### Configurazione automatica della rete
La rete verrà configurata automaticamente al rilevamento di un collegamento attivo. Adattate le seguenti linee a seconda del nome dell'interfaccia di rete presente se necessario.

~~~
allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug ens3
iface ens3 inet dhcp
~~~

### Configurazione statica della rete

Oppure configurate la rete staticamente.

~~~
auto eth0
iface eth0 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1

auto ens3
iface ens3 inet static
        address 192.168.1.2
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

## Configurazione manuale delle connessioni senza fili
La configurazione delle reti senza fili è molto simile a quella delle reti cablate con l'aggiunta dell'impostazione degli eventuali dettagli di autenticazione. Qualora necessitiate della configurazione automatica delle reti senza fili, dovreste usare il gestore di reti wicd.

Modificate il file interfaces per impostare le reti senza fili.

`root@debian:~# editor /etc/network/interfaces`

Adattate il file interfaces alle vostre interfacce di rete e alla vostra rete.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
