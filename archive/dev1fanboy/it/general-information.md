# Informazioni Generali
Questa pagina fornisce informazioni su Devuan e guide generali al suo utilizzo. Queste costituiscono le basi che chiunque voglia usare Devuan deve conoscere e sono adatte a tutti coloro che hanno già familiarità con Debian.

## Architetture supportate
* amd64
* i386
* armel
* armhf
* arm64

## Versioni disponibili
* ASCII (equivalente a Debian Stretch)
* Beowulf (versione stabile, equivalente a Debian Buster)
* Ceres (equivalente a Debian Sid)

## Differenze da Debian
Devuan rimuove systemd dall'init di default, e non si serve di systemd-shim per ovviare all'uso di systemd, invece adotta elogind per alcune delle funzionalità che richiedono systemd. Viene usato eudev al posto di udev, ormai parte delle sorgenti systemd. Sono state cambiate e/o aggiunte anche altre cose, come l'aspetto grafico e del software libero sviluppato dalla nostra comunità.

## Come procurarsi e verificare le immagini d'installazione
Cercate e scaricate le immagini d'installazione da [files.devuan.org](https://files.devuan.org/) o da uno dei mirror. Le immagini embedded sono nella [cartlla embedded](https://files.devuan.org/devuan_beowulf/embedded) di ogni versione. Dovreste scaricare anche i file `SHA256SUMS` e `SHA256SUMS.asc` dalla stessa directory.

Prima di tutto verificate che le immagini non siano compromesse.

`user@GNU/Linux:~$ sha256sum --ignore-missing -c SHA256SUMS`

A questo punto avete bisogno di importare le [chiavi di verifica](https://files.devuan.org/devuan-devs.gpg) degli sviluppatori Devuan.

`user@GNU/Linux:~$ gpg --import devuan-devs.gpg`

Accertatevi che il file SHA256SUMS sia effettivamente verificato dagli sviluppatori Devuan.

`user@GNU/Linux:~$ gpg --verify SHA256SUMS.asc`

Se la verifica ha esito positivo allora potrete scrivere le immagini sul vostro supporto avviabile.

## Scrittura di un'immagine su supporto avviabile

Tutte le ISO Devuan sono ISO ibride e possono quindi essere scritte su un disco USB per mezzo di dd.

`root@GNU/Linux:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

Se non disponete di un disco USB inutilizzato potete scrivere l'immagine su di in CD o un DVD per mezzo di wodim.

`user@GNU/Linux:~$ wodim dev=/dev/sr0 -eject filename.iso`

### Immagini embedded

Se necessitate di scrivere un'immagine embedded su di una scheda sd avrete bisogno dell'utility xz.

`root@GNU/Linux:~# apt-get install xz-utils`

Decompressione dell'immagine.

`user@GNU/Linux:~$ xz -d <filename>.img.xz`

Seguente scrittura sulla scheda sd.

`root@GNU/Linux:~# dd if=filename.img of=/dev/sdX && sync`

Se avete una scheda sunxi allora scrivete sulla sd anche [l'immagine u-boot](https://files.devuan.org/devuan_beowulf/embedded/u-boot/).

`root@GNU/Linux:~# dd if=<imagename>.bin of=/dev/sdX bs=1024 seek=8 && sync`

## Modifica dell'ordine di avvio del BIOS
Per poter avviare il sistema dal nuovo supporto è necessario modificare l'ordine di avvio UEFI/BIOS. In questa sede non è possibile descrivere la procedura esatta in quanto essa varia da un UEFI/BIOS all'altro, comunque di solito è semplice: Basta premere il tasto `Delete` prima della procedura di POST.

## Installazione
Se vi è venuta voglia di installare Devuan e avete già scritto il file immagine su un DVD o un CD, potete [seguire la guida all'installazione](devuan-install.md) per ottenere un sistema Devuan funzionante. Come sempre ricordatevi di fare un backup dei vostri dati!

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
