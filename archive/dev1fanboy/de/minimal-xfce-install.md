# Minimale XFCE-Installation
Dieses Dokument beschreibt, wie Du eine minimale XFCE4-Installation zusammen mit einigen optionalen Extras durchführst, die normalerweise gewünscht sind.

## Installieren von XFCE
Installiere die Kernpakete, die ausreichen, damit Du Deine neue Desktopumgebung verwenden kannst.

`root@devuan:~# apt-get install xfce4-panel xfdesktop4 xfwm4 xfce4-settings xfce4-session`

Du kannst auch folgende Pakete für einen vollständigeren Desktop verwenden.

`root@devuan:~# apt-get install tango-icon-theme xfwm4-themes xfce4-terminal xfce4-appfinder thunar xfce4-power-manager ristretto`

### Hinzufügen von Unterstützung für automatisches Mounten
Installiere die erforderlichen Pakete für Thunar (den xfce-Dateimanager), um automatisches Mounten zu unterstützen.

`root@devuan:~# apt-get install thunar-volman gvfs policykit-1`

Nachdem Du Deine Desktop-Umgebung gestartet hast, kannst Du nun mit dem xfce-Einstellungsmanager das automatische Mounten konfigurieren.

### Hinzufügen eines Displaymanagers
Wenn ein Display-Manager benötigt wird, empfehle ich, slim zu verwenden, was der Standard in Devuan ist.

`root@devuan:~# apt-get install slim`

Führe nun update-alternatives aus, um den x-session-manager auf xfce4-session einzustellen.

`root@devuan:~# update-alternatives --config x-session-manager`

### Verwendung von XFCE ohne einen Displaymanager
Melde dich bei einem regulären Benutzerkonto an der Konsole an und verwende das Skript startxfce4.

`user@devuan:~$ startxfce4`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>