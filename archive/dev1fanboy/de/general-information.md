# Allgemeine Informationen
Diese Seite bietet allgemeine Informationen sowie Informationen über Devuan. Dies sind die Grundlagen, die jeder kennen sollte, wenn er Devuan benutzen will, und sie sollten für jeden geeignet sein, der Debian noch nicht kennt.

## Unterstützte Architekturen
* amd64
* i386
* armel
* armhf
* arm64

## Zielzweige
* Jessie (stabile Version, äquivalent zu Debian Jessie)
* ASCII (äquivalent zu Debian Stretch)
* Ceres (äquivalent zu Debian Sid)

## Unterschiede zu Debian
Devuan entfernt systemd als Standard-Init und verlässt sich nicht auf systemd-shim, um systemd zu vermeiden. Seit dem ASCII-Release verwenden wir eudev als Standard-Gerätemanager anstelle von udev, da es jetzt Teil der systemd-Quellen ist. Wir fügen auch einige eigene Änderungen hinzu, wie zum Beispiel Grafiken und Oberflächengstaltung, kostenlose Software, die in unserer Gemeinschaft entwickelt wurde, und einige Pakete, die für unsere Benutzerbasis geeignet sind. 

## Installationsmedien abrufen und überprüfen
Finde und lade dein Installationsmedium herunter von [files.devuan.org](https://files.devuan.org/) oder einem der Spiegelserver. Du solltest ebenso die `SHA256SUMS` und `SHA256SUMS.asc` Dateien aus dem gleichen Verzeichnis wie die Installationsdatei herunterladen. 

Überprüfe zuerst die Integrität des/der Installationsdatei(en).

`user@GNU/Linux:~$ sha256sum --ignore-missing -c SHA256SUMS`

Du musst nun die [Signaturschlüssel](https://files.devuan.org/devuan-devs.gpg) der Devuan-Entwickler holen und sie importieren.

`user@GNU/Linux:~$ gpg --import devuan-devs.gpg`

Jetzt überprüfe, ob die SHA256SUMS-Datei von den Devuan-Entwicklern signiert wurde.

`user@GNU/Linux:~$ gpg --verify SHA256SUMS.asc`

Wenn die Signatur als gut gemeldet wird, kannst du die Dateien auf ein bootfähiges Medium schreiben.

## Ein Abbild auf ein bootfähiges Medium schreiben

Alle Devuan-ISO-Abbilder sind Hybrid-ISOs und können mit dd auf ein USB-Laufwerk geschrieben werden.

`root@GNU/Linux:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

Wenn Du kein zusätzliches USB-Laufwerk hast, kannst Du das Abbild mit wodim auf eine CD oder DVD schreiben.

`user@GNU/Linux:~$ wodim dev=/dev/sr0 -eject filename.iso`

### Abbilder für Einplatinencomputer

Wenn du ein Abbild für einen Einplatinencomputer (embedded) auf eine SD-Karte schreiben möchtest, musst Du das xz Dienstprogramm installieren.

`root@GNU/Linux:~# apt-get install xz-utils`

Entpacke das Abbild.

`user@GNU/Linux:~$ xz -d <filename>.img.xz`

Dann schreibe es auf die SD-Karte.

`root@GNU/Linux:~# dd if=filename.img of=/dev/sdX && sync`

## Ändern der BIOS-Startreihenfolge
Um von Deinem neuen Installationsmedium zu booten, musst Du die BIOS-Startreihenfolge ändern. Es ist nicht möglich, die genauen Schritte dafür zu beschreiben, da jedes BIOS anders ist. Du musst die BIOS- oder UEFI-Konfiguration aufrufen und die Startreihenfolge so ändern, dass CD/DVD-ROM- oder USB-Laufwerke vor den Festplattenlaufwerken erscheinen. Der Aufruf des BIOS ist normalerweise so einfach wie das Drücken der `Entf` Taste vor dem Selbsttest.

## Installationsanleitung
Das Wiki der Devuan-Freunde hat eine [Installationsanleitung](https://friendsofdevuan.org/doku.php/community:installing_devuan_1.0_jessie), der Du folgen kannst um von deinem Startmedium zu installieren. Denke immer daran, zuerst Deine Daten zu sichern!

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
