# Devuan ohne D-Bus
In diesem Dokument wird beschrieben, wie man D-Bus von Devuan entfernt, indem man zu einem schlanken Fenstermanager wechselt, einen Browser auswählt und eine alternative Lösung für das D-Bus-abhängige automatische mounten auswählt.

## Einen Fenstermanager wählen
Im Gegensatz zu Desktopumgebungen sind die meisten Fenstermanager nicht von D-Bus abhängig. Daher solltest Du einen Fenstermanager installieren.

* blackbox
* fluxbox
* fvwm
* fvwm-crystal
* openbox

### Installieren und Konfigurieren von Fluxbox
Wir werden Fluxbox verwenden, da er einfach und intuitiv ist.

`root@devuan:~# apt-get install fluxbox`

Mache Fluxbox zum Standardfenstermanager für Deinen Benutzer, wenn Du das Skript startx verwendest.

`user@devuan:~@ echo "exec fluxbox" >> .xinitrc`

Du kannst jetzt das startx-Skript aufrufen, um fluxbox zu verwenden.

`user@devuan:~@ startx`

Eine gute Option für einen Displaymanager ist WDM.

`root@devuan:~# apt-get install wdm`

## Auswahl eines Webbrowsers
Es gibt nur wenige Browser, die von dbus-Komponenten abhängig sind. Einige sind jedoch besser als andere. Hier sind einige Webbrowser, aus denen Du auswählen kannst.

* xombrero
* lynx
* links2
* dillo
* midori
* firefox-esr

Wir werden den bekannten Firefox ESR verwenden, da er die meisten Funktionen hat.

`root@devuan:~# apt-get install firefox-esr`

## D-Bus von Devuan entfernen

Wir können jetzt dbus von Devuan entfernen.

`root@devuan:~# apt-get purge dbus`

Wir sollten auch alle Pakete entfernen, die durch dbus-Entfernung unnötig wurden.

`root@devuan:~# apt-get autoremove --purge`

## Eine einfache Alternative zum automatischen Mounten
Ohne D-Bus ist für die meisten Dateimanager kein automatisches mounten verfügbar, da diese Teile D-Bus erfordern und einfachere Mount-Verfahren nicht immer implementiert sind. Wir richten Mount-Punkte für uns selbst ein, damit geeignetere Dateimanager die Laufwerke mit nur wenigen Klicks mounten können.

### Manuelle Einhängepunkte
Erstelle ein Verzeichnis für den neuen Einhängepunkt.

`root@devuan:~# mkdir /media/usb0`

Sichere Deine fstab, bevor Du fortfährst.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Jetzt können wir die fstab bearbeiten.

`root@devuan:~# editor /etc/fstab`

Wir müssen am Ende der fstab einen Einhängepunkt für ein USB-Laufwerk hinzufügen. Achte darauf, die Option "user" anzugeben, so dass Benutzer ohne Rootberechtigung das Laufwerk mounten können.

~~~
/dev/sdb1       /media/usb0     auto    user,noauto     0 0
~~~

Die Geräteknoten für USB-Festplatten variieren je nach Einrichtung. Du kannst herausfinden, welche Geräteknoten verwendet werden, indem Du das Laufwerk einsteckst und das Dienstprogramm "lsblk" aufrufst.

Schließe ein USB-Laufwerk an, um Deine Arbeit zu testen.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Einen Dateimanager auswählen
Für einen grafischen Dateimanager, der Laufwerke basierend auf Deiner fstab ein- und aushängt, kkannst Du Xfe verwenden.

`root@devuan:~# apt-get install xfe`

Ein interessanter und minimalistischer Dateimanager ist der ncurses-basierte Midnight Commander.

`root@devuan:~# apt-get install mc`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>