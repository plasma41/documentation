# Devuan GNU / Linux Installation, Upgrades und Minimalismus
Diese Dokumentation enthält Informationen zum Installieren, Aktualisieren und Migrieren zu Devuan und zum Verschlanken des Systems auf das Nötigste.
Das Wiki ist auch in verschiedenen nicht-englischen Sprachen verfügbar.

## Alle Versionen
[Allgemeine Informationen](general-information.md)  
[Devuan installieren](devuan-install.md)  
[Vollständige Festplattenverschlüsselung](full-disk-encryption.md)  
[Netzwerk Konfiguration](network-configuration.md)  
[Devuan ohne D-Bus](devuan-without-dbus.md)  
[D-Bus freie Software](dbus-free-software.md)  
[Minimale xorg Installation](minimal-xorg-install.md)  
[Minimale xfce Installation](minimal-xfce-install.md)

## ASCII
[Zu ASCII migrieren](migrate-to-ascii.md)  
[Upgrade auf ASCII](upgrade-to-ascii.md)

## Translations

Translations have been made available thanks to the wiki team.

[Read in English](../) **|** [Διαβάστε στα Ελληνικά](../el) **|** [Leer en Español](../es) **|** [Leggi in Italiano](../it) **|** [Leia em Português](../pt)

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
