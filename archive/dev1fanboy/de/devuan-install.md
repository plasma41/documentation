# Devuan Installationsanleitung

Dies ist eine leicht zu befolgende Anleitung zur Installation von Devuan von den CD/DVD-Images für unterstützte Hardware. Der allgemeine Ratschlag zu dieser Anleitung lautet, dass Sie Ihre Daten immer sichern sollten, bevor Sie beginnen.

## Inhalt
[Voraussetzungen](#voraussetzungen)  
[Unterstützte Architekturen](#unterstützte-architekturen)  
[Installationsabbilder](#installationsabbilder)  
[Devuan installieren](#devuan-installieren)

## Voraussetzungen

Zumindest sollten Sie wissen, wie Sie ein ISO-Image auf CD/DVD oder USB schreiben und Ihren Computer damit booten können. Für diejenigen, die bereits GNU/Linux oder ähnliches verwenden, können wir die Schritte dafür abdecken.

## Unterstützte Architekturen

* amd64
* i386

## Installationsabbilder

Dies sind derzeit die Möglichkeiten, Installationsabbilder zu erhalten.

* Direkt aus dem [Devuan Release-Archiv](https://files.devuan.org)
* Von Spiegelservern, die auf [devuan.org](https://www.devuan.org) aufgelistet sind und möglicherweise näher bei Ihnen sind
* Via [torrent](https://files.devuan.org/devuan_beowulf.torrent) für die Stable-Versionen

Wenn Sie Zugriff auf das Netzwerk haben, wird empfohlen, ein einzelnes vollständiges DVD-Image für die Installation zu verwenden, ansonsten sollten Sie den vollständigen Satz verwenden.

Bitte verwenden Sie möglichst Spiegelserver oder Torrents.

Diejenigen, die die Befehlszeile nicht verwenden, können [zur Installation springen](#devuan-installieren).

### Überprüfen der Integrität von Abbildern

Bevor Sie ein Abbld auf Ihren Wechseldatenträger schreiben, überprüfen Sie am besten die Integrität, damit Sie sicher sein können, dass das Abbild in einem guten Zustand ist. Dies vermeidet viele Probleme, die später bei der Installation auftreten können.

Laden Sie die `SHA256SUMS` aus dem [Release Archiv](https://files.devuan.org) herunter und überprüfen Sie die Abbildintegrität.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Überprüfen der Abbilder

Installationsabbilder, die von Devuan verteilt werden, sind so signiert, dass sie als von Devuan stammend verifiziert werden können. Durch die Überprüfung der Abbilder wissen Sie, dass sie vor dem Empfang nicht verändert wurden.

Holen Sie sich die Devuan-Entwickler [Signaturschlüssel](https://files.devuan.org/devuan-devs.gpg) und importieren Sie sie in Ihren Schlüsselbund.

`user@hostname:~$ gpg --import devuan-devs.gpg`

Verwenden Sie das signierte `SHA256SUMS.asc` aus dem Veröffentlichungsarchiv, um das Abbild zu überprüfen.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

Ein Bericht über eine gute Unterschrift zeigt an, dass alles in Ordnung ist.

### Ein Abbild auf eine CD/DVD oder ein USB-Laufwerk schreiben

Abbilder können mit wodim auf eine CD oder DVD geschrieben werden.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

Alle Devuan-ISO-Abbilder sind Hybrid-ISOs und können mit dd auf ein USB-Laufwerk geschrieben werden.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Devuan installieren

Für die Installation verwenden wir einen nicht-grafischen Installer. Dies ist in der Regel schneller als eine grafische Installation und benötigt weniger Ressourcen. Dies sollte sowohl für die Installation in einer virtuellen Maschine als auch auf physischer Hardware geeignet sein. Während dies für neue Benutzer einschüchternd sein kann, gibt es nichts zu befürchten, da Sie durch diesen Prozess geführt werden.

&nbsp;

**1) Booten Sie von der CD/DVD oder dem USB-Laufwerk und wählen Sie die normale `Install`-Option.**

![Devuan Installation Start](../img/firstboot.png)

&nbsp;

**2) In den nächsten Schritten werden Sie nach Ihrer Sprache, Ihrem Standort und dem Tastaturlayout gefragt.**

![Tastaturlayout](../img/keyboard.png)

&nbsp;

**3) Das Installationsprogramm konfiguriert das Netzwerk automatisch.
Drahtlose Netzwerkbenutzer müssen eine SSID und eine Passphrase angeben.
Sie werden dann aufgefordert, einen Hostnamen für Ihr neues System auszuwählen.
Seien Sie auf jeden Fall kreativ, aber denken Sie daran, keine Leerzeichen oder Sonderzeichen zu verwenden.**

![Hostname](../img/hostname.png)

&nbsp;

**4) Sie werden auch aufgefordert, einen Domainnamen anzugeben.
Wenn Sie das nicht brauchen oder nicht wissen, was es für Sie ist, sollten Sie es leer lassen.**

![Domainname](../img/domain.png)

&nbsp;

**5) Es wird empfohlen, dass Sie ein Root-Passwort für Devuan festlegen.
Es ist eine gute Sicherheitsvorkehrung, ein starkes Passwort zu verwenden.
Sie müssen Ihr Passwort erneut eingeben, um sicherzustellen, dass Sie es richtig eingegeben haben.**

![Root Passwort](../img/rootpw.png)

&nbsp;

**6) Sie werden nun aufgefordert, ein Benutzerkonto zu konfigurieren, was Sie in den meisten Fällen tun sollten.
Falls nicht unbedingt nötig, lassen Sie Ihren vollständigen Namen leer und geben Sie einen Nutzernamen an.**

![Benutzername](../img/username.png)

&nbsp;

**7) Sie müssen ein Passwort eingeben und es erneut eingeben, wie Sie es zuvor für root getan haben.**

![Benutzer Passwort](../img/userpw.png)

&nbsp;

**8) Das Installationsprogramm stellt jetzt die Uhr mit [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol) ein. Geben Sie Ihre Zeitzoneninformationen ein und fahren Sie mit der Installation fort.**

![Uhr einstellen](../img/clock.png)

&nbsp;

**9) Bevor Sie Devuan installieren können, muss die Festplatte partitioniert werden.
Wenn es als Option verfügbar ist, empfiehlt es sich, den größten kontinuierlichen Speicherplatz zu verwenden.
Dadurch werden vorhandene Partitionen beibehalten und nicht geändert.
Andernfalls sollten Sie die gesamte Festplatte verwenden, wenn Sie keine anderen Daten speichern müssen.**

**Wenn Sie eine vollständige Festplattenverschlüsselung benötigen, lesen Sie [hier](full-disk-encryption.md), bevor Sie fortfahren.**

![Größten Speicherplatz verwenden](../img/partdisks1.png)

&nbsp;

**10) Die Auswahl aller Dateien in einer Partition ist eine sinnvolle Option für Neueinsteiger. Manuelle Partitionierung von Festplatten ist nicht Gegenstand dieser Diskussion.** 

![Alle Dateien in einer Partition](../img/partdisks2.png)

&nbsp;

**11) Es ist Zeit, diese Partitionen auf die Festplatte zu schreiben und sie mit Dateisystemen zu formatieren.
Wenn Sie mit den Änderungen zufrieden sind, wählen Sie "Auf Festplatte schreiben" und fahren Sie fort.
Sie müssen dies bestätigen, bevor die Änderungen vorgenommen werden.**

![Änderungen auf Platte schreiben](../img/partdisks3.png)

&nbsp;

**12) Das Basissystem wird nun installiert. Abhängig von Ihrer Hardware kann dies einige Zeit dauern.**

![Warten auf Installation des Basissystems](../img/installing.png)

&nbsp;

**13) Das Installationsprogramm fragt jetzt, ob Sie einen Netzwerkspiegelserver verwenden möchten.
Wenn Sie über Netzwerkzugriff verfügen, wird dies empfohlen, da Sie Zugriff auf die neuesten Paketversionen haben.
Wenn Sie einen Netzwerkspiegelserver verwenden möchten, wählen Sie Ihr Land aus der Liste.**

![Netzwerkspiegelserver verwenden](../img/mirror1.png)

&nbsp;

**14) Jeder der angebotenen Spiegelserver ist in Ordnung. Wenn sich ein Spiegelserver in Ihrer Nähe befindet, werden Sie beim Installieren von Paketen automatisch umgeleitet.**

![Netzwerkspiegelserver verwenden](../img/mirror2.png)

&nbsp;

**15) Devuan kann den Beliebtheitswettbewerb (popcon) verwenden, um Informationen über die am häufigsten verwendeten Pakete zu sammeln.
Dies geschieht ausschließlich auf Opt-In-Basis und sammelt nur Statistiken über Pakete, die von hier aus installiert werden.**

![Beliebtheitswettbewerb](../img/popcon.png)

&nbsp;

**16) Die Installation bietet einige voreingestellte Pakete, aus denen Sie auswählen können.
Im Allgemeinen sind die Standardeinstellungen eine gute Option, und Sie sollten bei denen bleiben.**

![Software Auswahl](../img/tasksel.png)

&nbsp;

**17) Das Installationsprogramm installiert nun die von Ihnen ausgewählten Pakete. Dies wird ein wenig dauern.**

![Software installieren](../img/retrieving.png)

&nbsp;

**18) Jetzt, da alle benötigte Software installiert ist, wird der GRUB Bootloader installiert.
Dadurch kann das Betriebssystem nach der Installation gestartet werden.
Wenn Sie aufgefordert werden, in den MBR zu installieren, sollten Sie dies normalerweise tun.**

**Manche Setups erfordern keine Konfiguration und die Installation wird nun beendet.**

![Bootloader installieren](../img/grub-mbr.png)

&nbsp;

**19) Es ist wichtig, den richtigen Speicherort für den Bootloader zu wählen.
Er sollte nicht auf einer Partition installiert werden, sondern auf dem MBR-Bereich, der sich auf der Festplatte befindet.**

**In diesem Fall ist `/dev/sda` die einzige Festplatte und wir installieren ihn dort.**

![Speicherort für Bootloader wählen](../img/grubtohdd.png)

&nbsp;

**20) Nachdem die Installation abgeschlossen ist, entfernen Sie Ihr Installationsmedium, um mit der grafischen Umgebung von Devuan fortzufahren.**

![Installation beenden](../img/finish.png)


---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>
