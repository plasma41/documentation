# Migrate to Devuan ASCII
This document describes how to migrate to Devuan ASCII from Debian Jessie or Stretch. Currently migration to ASCII from Debian Jessie or Stretch is not straight forward if you use GNOME or network manager due to some held packages, and each respective migration is a little different but this should work round the issues in both cases.

## Migrating from the GNOME desktop
Because there is no transitional package yet GNOME users will need to install xfce4 and slim manually. Choose slim as the default display manager when prompted.

`root@debian:~# apt-get install xfce4 slim`

Set the session manager to startxfce4 so that the new desktop can be used later.

`root@debian:~# update-alternatives --config x-session-manager`

## Replacing network manager with wicd

For the migration we need to use wicd instead of network manager. Due to a compatibility problem with Debian, those migrating from Stretch remotely **must** use manual [network configuration](network-configuration.md) instead, otherwise you will lose access to the remote host during migration.

`root@debian:~# apt-get install wicd`

Make sure network manager is removed from the boot process.

`root@debian:~# update-rc.d -f network-manager remove`

Wireless connections should now be configured to automatically connect with wicd. Stop network manager before configuring your network.

`root@debian:~# /etc/init.d/network-manager stop`

## Perform the migration

Devuan uses sysvinit by default so we will install this now.

`root@debian:~# apt-get install sysvinit-core`

A reboot is required to switch sysvinit to pid1 so we can remove systemd.

`root@debian:~# reboot`

We can now remove systemd without any complaints.

`root@debian:~# apt-get purge systemd`

Edit the sources.list file so that we can switch to the Devuan repositories.

`root@debian:~# editor /etc/apt/sources.list`

Add the Devuan mirrors with the ASCII branch name. Comment out any other lines.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Update the package indexes so we can install the Devuan archive keyring.

`root@debian:~# apt-get update`

Install the Devuan keyring so packages can be authenticated from this point onwards.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Update the package indexes again so that they are authenticated with the keyring.

`root@debian:~# apt-get update`

Finally we can migrate to Devuan.

`root@debian:~# apt-get dist-upgrade`

## Post migration tasks
systemd components should now be removed from the system.

`root@devuan:~# apt-get purge systemd-shim`

If you are not using D-Bus or xorg you might be able to remove libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Purge any orphaned packages left over from your previous Debian install.

`root@devuan:~# apt-get autoremove --purge`

This is a good time to clean out old package archives left over from your Debian system.

`root@devuan:~# apt-get autoclean`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
