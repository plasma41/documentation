# Devuan install guide

This is an easy to follow guide to installing Devuan from the CD/DVD images for supported hardware. The general advise accompanying this guide is that you should always backup your data before you begin.

## Contents
[Prerequisites](#prerequisites)  
[Supported architectures](#supported-architectures)  
[Installation images](#installation-images)  
[Installing Devuan](#installing-devuan)

## Prerequisites

At minimum you should know how to write an ISO image to CD/DVD/USB and make your computer boot from it. For those already using GNU/Linux or similar we can cover the steps for that.

## Supported architectures

* amd64
* i386

## Installation images

These are currently the ways of getting installation images.

* Directly from the [Devuan release archive](https://files.devuan.org)
* From mirrors listed on [devuan.org](https://www.devuan.org) that may be closer to you
* Via [torrent](https://files.devuan.org/devuan_beowulf.torrent) for the stable releases

If you have access to the network it's suggested to use a single full DVD image for installation, otherwise you will want the full set.

Please use mirrors or torrents where possible.

Those not using the command line can [skip to installation](#installing-devuan).

### Check the integrity of images

Before you write an image to your removable device, it's best to check the integrity so that you can be sure the image is in a good state. This avoids many problems that may later occur during installation otherwise.

Download the `SHA256SUMS` from the [release archive](https://files.devuan.org) and verify image integrity.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Verify the images

Installation images distributed by Devuan are signed so that they can be verified as coming from Devuan. Verifying images lets you know they have not been altered prior to you receiving them.

Get the Devuan developers [signing keys](https://files.devuan.org/devuan-devs.gpg) and import them to your keychain.

`user@hostname:~$ gpg --import devuan-devs.gpg`

Use the signed `SHA256SUMS.asc` from the release archive to verify the image.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

A report of a good signature indicates everything is fine.

### Writing an image to a CD/DVD or USB drive

Images can be written to a CD or DVD using wodim.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

All Devuan ISO images are hybrid ISOs and may be written to a USB drive using dd.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Installing Devuan

Devuan installation is presented via console framebuffer screens and responses are given by keyboard. Whilst this may seem intimidating for new users, there is nothing to be feared as you will be guided through this process.

&nbsp;

**1) Boot from the CD/DVD/USB drive and choose the `Install` option.**

![Devuan install first boot](../img/01start.png)

&nbsp;

**2) The next few steps will ask about your language, location and keyboard layout.**

![Keyboard layout](../img/02keyboard.png)

&nbsp;

**3) The installer will automatically configure the network, wireless network users will have to provide  
an SSID and passphrase.**

**You will then be asked to choose a host name for your new system.You can be creative but spaces and  
special characters will not be accepted.**

![Hostname](../img/03hostname.png)

&nbsp;

**4) You will also be asked to provide a domain name. If you don't need this or don't know what it is  
for you should leave it blank.**

![Domain name](../img/04domain.png)

&nbsp;

**5) It's recommended that you set a root password for Devuan. It's good security practice to use a  
strong password. You will have to type your password again to make sure that you've set it correctly.**

![Root password](../img/05rootpw.png)

&nbsp;

**6) You will now be asked to configure a user account, which you should do in most cases. Unless there  
is a need to do otherwise leave your full name blank and continue to providing a username.**

![User name](../img/06username.png)

&nbsp;

**7) You will need to enter a password and type it again as you did before for root.**

![User password](../img/07userpw.png)

&nbsp;

**8) The installer will now set the clock using [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol). Enter your time zone information to continue with  
the install.**

![Configure the clock](../img/08ntp.png)

&nbsp;

**9) Before you can install Devuan the disk need to be partitioned. If it's available as an option choosing  
to use the largest continuous space is recommended. This will preserve existing partitions and not  
alter them. Otherwise you should use the whole disk if you have no data you need to save.**

**If you need full disk encryption see [here](full-disk-encryption.md) before continuing.**

![Use largest space](../img/09part1.png)

&nbsp;

**10) Choosing all files in one partition is a sensible option for newcomers. Manual disk partitioning is  
beyond the scope of this discussion.** 

![All files in one partition](../img/10part2.png)

&nbsp;

**11) It's time to write those partitions to the disk and format them with file systems. If you are happy  
with the changes choose write to disk and continue. You will be given a last chance to back out before  
the changes are made.**

![Write changes to disk](../img/11part3.png)

&nbsp;

**12) The base system will now install. Depending on your hardware this may take some time.**

![Waiting for the base system install](../img/12install.png)

&nbsp;

**13) In the case you have no Internet access or don't want to use a mirror, it's recommended to scan  
additional CD's so that you have more packages available to install.**

![Scan another CD](../img/13scancd.png)

&nbsp;

**14) If you are installing from a full CD/DVD image, the installer will now ask if you want to choose a  
network mirror. If you have Internet access it's recommended to use a mirror so that you have the  
latest versions of packages. For an offline installation you should continue without a mirror.**

![Use a network mirror](../img/14mirror1.png)

&nbsp;

**15) Either of the provided mirrors will be fine. If there is a mirror close to you then you will be  
redirected automatically when installing packages.**

![Choose a network mirror](../img/15mirror2.png)

&nbsp;

**16) Devuan can use popularity contest (popcon) to collect information about the most used packages.  
This is purely on an opt-in basis and will only collect statistics about packages that are installed from  
here on in.**

![Popularity contest](../img/16popcon.png)

&nbsp;

**17) The defaults here are sufficient to get a working xfce desktop. Note that you don't need to select  
xfce4 explicitly as it's the default, but you may choose another desktop. Others can be excluded or  
included based on your needs.**

![Software selection](../img/17tasksel.png)

&nbsp;

**18) The installer will now install the packages you selected. This will take a little time.**

![Installing software](../img/18retrieve.png)

&nbsp;

**19) You will now be asked to choose your preferred init system. The default in Devuan is sysvinit.**

![Choose an init system](../img/19init.png)

&nbsp;

**20) Now that all the software you need is installed, the GRUB bootloader will be installed. This allows  
the operating system to boot after installation. If you are asked to install to the MBR then you should  
do this usually.**

**Some setups will require no configuration and the install will now finish.**

![Installing the grub boot loader](../img/20grub1.png)

&nbsp;

**21) It's important to choose the correct bootloader location. It should not be installed to a partition,  
but to the MBR area which is located on the hard disk.**

**In this case `/dev/sda` is the only hard disk so we will install it there.**

![Choose bootloader location](../img/21grub2.png)

&nbsp;

**22) The install is finished, remove your installation media to continue to boot to your Devuan install.**

![Finish the install](../img/22finish.png)

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
