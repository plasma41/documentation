# Minimal XFCE install
This document describes how to perform a minimal xfce4 install, along with some optional extras for a more complete desktop.

## Installing XFCE
Install the core packages that will be enough to allow you to start using the XFCE desktop environment.

`root@devuan:~# apt-get install xfce4-panel xfdesktop4 xfwm4 xfce4-settings xfce4-session`

The following packages are useful for a more complete desktop.

`root@devuan:~# apt-get install xfce4-terminal xfce4-appfinder xfce4-power-manager thunar ristretto cinnabar-icon-theme`

### Adding support for auto-mounting

Install the necessary packages for thunar (the XFCE file manager) to support auto-mounting of drives.

`root@devuan:~# apt-get install thunar-volman gvfs policykit-1`

When you next login and start your desktop you can use the XFCE settings manager to configure auto-mounting.

### Adding a display manager
If you want a display manager you would normally use slim in Devuan.

`root@devuan:~# apt-get install slim`

Now run update-alternatives to set the x-session-manager to xfce4-session.

`root@devuan:~# update-alternatives --config x-session-manager`

### Using XFCE without a display manager
Login to a regular user account at the console and use the startxfce4 script.

`user@devuan:~$ startxfce4`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
