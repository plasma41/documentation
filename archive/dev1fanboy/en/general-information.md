# General information
This page provides general usage and information about Devuan. These are the basics that everybody should know if they want to use Devuan, and is intended to be suitable for anybody not already familiar with Debian.

## Architectures

Devuan is available on a number of architectures including PC's and embedded devices.

* amd64
* i386
* armel
* armhf
* arm64

## Target branches

As with Debian we have a stable branch, but since releases are based on Debian stable we do not use the concept of a testing branch.

* ASCII (equivalent to Debian Stretch)
* Beowulf (stable release, equivalent to Debian Buster)
* Ceres (equivalent to Debian Sid)

## Differences from Debian
Devuan removes systemd as the default init and does not rely on systemd-shim to avoid using systemd, instead we use elogind to provide some functions that would otherwise need systemd. We also use eudev in place of udev because it's now part of the systemd sources. Devuan has it's own own artwork and theming and some free software available from our community.

## Getting and verifying installation images
Find and download your image(s) from [files.devuan.org](files.devuan.org) or preferably one of the [mirrors](https://devuan.org/get-devuan), embedded images can be found in the [embedded directory](https://files.devuan.org/devuan_beowulf/embedded) for each release. You should also download the `SHA256SUMS` and `SHA256SUMS.asc` files from the same directory as the images.

Now verify the integrity of the image(s).

`user@GNU/Linux:~$ sha256sum --ignore-missing -c SHA256SUMS`

You will now need to get the Devuan developers [signing keys](https://files.devuan.org/devuan-devs.gpg) and import them.

`user@GNU/Linux:~$ gpg --import devuan-devs.gpg`

Then verify the SHA256SUMS file is the one signed by the Devuan developers.

`user@GNU/Linux:~$ gpg --verify SHA256SUMS.asc`

If the signature is reported as good then you can write the image(s) to your bootable media.

## Writing an image to bootable media

All Devuan ISO images are hybrid ISOs and may be written to a USB drive using dd.

`root@GNU/Linux:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

If you don't have a spare USB drive you can write the image to a CD or DVD using wodim.

`user@GNU/Linux:~$ wodim dev=/dev/sr0 -eject filename.iso`

### Embedded images

If you want to write an embedded image to an sdcard you will need to install the xz utility.

`root@GNU/Linux:~# apt-get install xz-utils`

Decompress the image.

`user@GNU/Linux:~$ xz -d <filename>.img.xz`

Then write it to the sdcard.

`root@GNU/Linux:~# dd if=filename.img of=/dev/sdX && sync`

If your board is a sunxi board then also flash the [u-boot image](https://files.devuan.org/devuan_beowulf/embedded/u-boot/) to the sdcard.

`root@GNU/Linux:~# dd if=<imagename>.bin of=/dev/sdX bs=1024 seek=8 && sync`

## Changing the BIOS boot order
In order to boot from your new install media you will have to change the UEFI/BIOS boot order. Every UEFI/BIOS is different so it's not possible to cover the steps for this, however you can usually enter the setup screen using the `Delete` key during POST.

## Installation

If you're now interested in installing Devuan and have written your image to a CD/DVD, you can [follow the installation guide](devuan-install.md) to get your system up and running with Devuan.

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
