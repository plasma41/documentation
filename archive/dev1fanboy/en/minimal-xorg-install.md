# Minimal xorg install
This document describes how to perform a minimal xorg installation with some optional good defaults.

## Install the core xorg packages
First install the minimal set of packages needed for xorg.

`root@devuan:~# apt-get install xserver-xorg-video-dummy xserver-xorg-input-void xinit x11-xserver-utils`

Installing the void input driver and dummy video driver prevents APT installing all available drivers. This way you can install only the drivers you need.

## Install your graphics driver
You should now install the display driver for your hardware.

Commonly used display drivers include:

* xserver-xorg-video-intel (intel)
* xserver-xorg-video-nouveau (nvidia)
* xserver-xorg-video-openchrome (via)
* xserver-xorg-video-radeon (amd)
* xserver-xorg-video-vesa (generic display driver)

For example if you have an amd graphics chip you should install the radeon driver.

`root@devuan:~# apt-get install xserver-xorg-video-radeon`

If you are unsure of your driver you can use the vesa driver for now until you learn more about your hardware. The vesa driver will work with all VESA compliant displays and is also useful and is also useful as a fallback in case of problems with the primary driver.

`root@devuan:~# apt-get install xserver-xorg-video-vesa`

For graphics chips not mentioned above you can search the repository to find the correct display driver for your hardware.

`root@devuan:~# apt-cache search xserver-xorg-video-.* | pager`

## Installing your input drivers

In many cases your input devices will just work because of the presence of the libinput driver. If you have specific needs or you're not happy with this you can install other drivers.

If you have a mouse and keyboard setup, you can install drivers for them separately if you prefer.

`root@devuan:~# apt-get install xserver-xorg-input-mouse xserver-xorg-input-kbd`

For a synaptics touchpad you will often need to install the synaptics driver alongside the keyboard driver.

`root@devuan:~# apt-get install xserver-xorg-input-synaptics`

If you need other input drivers you can search the repository to find them.

`root@devuan:~# apt-cache search xserver-xorg-input-.* | pager`

## Installing optional extras

It's a very good idea to install the basic set of fonts for xorg.

`root@devuan:~# apt-get install xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable`

ASCII users who want to take advantage of opengl should install the mesa support packages, if you're using a free software driver like the ones mentioned above. Beowulf users do not need to do anything here.

`root@devuan:~# apt-get install libgl1-mesa-dri mesa-utils`

ASCII and later may need the xserver-xorg-legacy package to manage permissions for the xserver. Note that this is a setuid wrapper.

`root@devuan:~# apt-get install xserver-xorg-legacy`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
