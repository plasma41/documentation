# Οδηγός εγκατάστασης Devuan

Αυτός είναι ένας εύκολος οδηγός για την εγκατάσταση του Devuan από μια εικόνα CD/DVD σε υποστηριζόμενο υλικό. Η γενική συμβουλή που συνοδεύει αυτό τον οδηγό, είναι ότι πρέπει πάντα να διασφαλίζετε τα δεδομένα σας πριν ξεκινήσετε.

## Περιεχόμενα
[Προαπαιτούμενα](#prerequisites)  
[Υποστηριζόμενες αρχιτεκτονικές](#supported-architectures)  
[Εικόνες εγκατάστασης](#installation-images)  
[Εγκατάσταση Devuan](#installing-devuan)

## Προαπαιτούμενα

Κατ' ελάχιστον θα πρέπει να ξέρετε πως να γράψετε μια εικόνα ISO σε ένα CD/DVD-ROM ή δίσκο USB, και να εκκινήσετε τον υπολογιστή σας από αυτό. Για όσους ήδη χρησιμοποιούν GNU/Linux ή παρόμοιο μπορούμε να καλύψουμε τα βήματα γι' αυτό.

## Υποστηριζόμενες αρχιτεκτονικές

* amd64
* i386

## Εικόνες εγκατάστασης

Εικόνες εγκατάστασης μπορείτε να λάβετε με τους παρακάτω τρόπους. 

* Απευθείας από το [αρχείο εκδόσεων του Devuan](https://files.devuan.org)
* Από καθρέπτες(mirrors) που μπορεί να είναι πιο κοντά σας, στο [devuan.org](https://www.devuan.org)
* Μέσω [torrent](https://files.devuan.org/devuan_beowulf.torrent) για τις σταθερές εκδόσεις

Αν έχετε πρόσβαση στο διαδίκτυο προτείνεται η χρήση μιας πλήρης εικόνα DVD για εγκατάσταση, αλλιώς θα χρειαστείτε το πλήρες σύνολο.

Παρακαλούμε χρησιμοποιείστε τους καθρέπτες ή torrents όποτε αυτό είναι δυνατό.

Όσοι δεν χρησιμοποιούν τη γραμμή εντολών μπορούν να [μεταπηδήσουν στην εγκατάσταση](#installing-devuan).

### Έλεγχος ακεραιότητας των εικόνων

Προτού εγγράψετε μια εικόνα εγκατάστασης σε αφαιρούμενη συσκευή, καλύτερα να ελέγχετε την ακεραιότητα της εικόνας για να βεβαιωθείτε ότι είναι σε καλή κατάσταση. Αυτό βοηθάει να αποφεύγονται πολλά προβλήματα που μπορεί να τύχουν αργότερα κατά την εγκατάσταση.

Μεταφορτώστε το `SHA256SUMS` από το [αρχείο εκδόσεων](https://files.devuan.org) και επαληθεύστε την ακεραιότητα της εικόνας.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Επαλήθευση εικόνων

Εικόνες εγκατάστασης που διανείμονται από το Devuan είναι υπογεγραμμένες ώστε να μπορούν να επαληθευτούν για το αν όντως προέρχονται από το Devuan. Η επαλήθευση των εικόνων σας επιτρέπει να γνωρίζεται πως δεν έχουν υποστεί αλλαγές πριν τις λάβετε.

Λάβετε τα [κλειδιά υπογραφής](https://files.devuan.org/devuan-devs.gpg) των προγραμματιστών του Devuan και εισάγετε τα στο αποθετήριο κλειδιών σας.

`user@hostname:~$ gpg --import devuan-devs.gpg`

Χρησιμοποιείστε το υπογεγραμμένο `SHA256SUMS.asc` από το αρχείο εκδόσεων για την επιβεβαίωση της εικόνας.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

Η αναφορά μιας καλής υπογραφής δείχνει πως όλα είναι καλά.

### Εγγραφή μιας εικόνας στο δίσκο CD/DVD ή USB.

Εικόνες μπορούν να εγγραφούν σε ένα CD ή DVD με χρήση του wodim.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

Όλες οι εικόνες Devuan ISO είναι υβριδικά ISO και μπορούν να εγγραφούν σε ένα USB δίσκο με χρήση της εντολής dd.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Εγκατάσταση Devuan

Για την εγκατάσταση θα χρησιμοποιήσουμε ένα μη γραφικό πρόγραμμα εγκατάστασης. Αυτό είναι πιο γρήγορο από ένα γραφικό πρόγραμμα εγκατάστασης και απαιτεί λιγότερους πόρους. Είναι κατάλληλο για μια εγκατάσταση σε εικονικό μηχάνημα καθώς και σε φυσικό μηχάνημα. Παρόλο που αυτό μπορεί να μην είναι ελκυστικό για νέους χρήστες, δεν υπάρχει τίποτε να φοβηθείτε αφού θα καθοδηγείστε μέσω της διαδικασίας.

&nbsp;

**1) Εκκινήστε από το δίσκο CD/DVD ή USB, και επιλέξτε την επιλογή κανονικής `Εγκατάστασης (Install)`.

![Πρώτη εκκίνηση εγκατάστασης Devuan](../img/firstboot.png)

&nbsp;

**2) Τα επόμενα βήματα θα σας ρωτήσουν για τη γλώσσα, τοποθεσία και διάταξη πληκτρολογίου.**

![Διάταξη πληκτρολογίου](../img/keyboard.png)

&nbsp;

**3) Το πρόγραμμα εγκατάστασης θα ρυθμίσει αυτόματα το δίκτυο, αλλά οι χρήστες ασύρματου δικτύου θα πρέπει να παράσχουν το SSID και το κωδικό εισόδου. 
Κατόπιν θα σας ζητηθεί να επιλέξετε όνομα υπολογιστή για το νέο σας σύστημα. Μπορείτε να είστε δημιουργικοί αλλά θυμηθείτε να μη χρησιμοποιήσετε κενά ή ειδικούς χαρακτήρες.**

![Όνομα υπολογιστή](../img/hostname.png)

&nbsp;

**4) Θα σας ζητηθεί επίσης να παρέχεται ένα όνομα τομέα. Εάν δε το χρειάζεστε ή δε ξέρετε τι είναι, αφήστε το κενό.**

![Όνομα τομέα](../img/domainname.png)

&nbsp;

**5) Προτείνεται να ρυθμίσετε κωδικό πρόσβασης για τον root στο Devuan. Για λόγους ασφάλειας, χρησιμοποιείστε ένα καλό κωδικό. Θα πρέπει να επαναλάβετε τη πληκτρολόγηση του κωδικού για να βεβαιωθείτε πως το θέσατε σωστά.**

![Κωδικός πρόσβασης root](../img/rootpassword.png)

&nbsp;

**6) Θα σας ζητηθεί τώρα να ρυθμίσετε ένα λογαριασμό χρήστη, κάτι που θα πρέπει να κάνετε στις περισσότερες των περιπτώσεων. 
Εκτός εάν υπάρχει λόγος να κάνετε διαφορετικά, αφήστε το πλήρες όνομά σας κενό και συνεχίσετε με τη παροχή ενός ονόματος χρήστη.**

![Όνομα χρήστη](../img/username.png)

&nbsp;

**7) Θα χρειαστεί να εισάγετε έναν κωδικό πρόσβασης και να τον πληκτρολογήσετε ξανά όπως κάνατε πριν για τον root.**

![Κωδικός πρόσβασης χρήστη](../img/userpassword.png)

&nbsp;

**8) Το πρόγραμμα εγκατάστασης θα ρυθμίσει τώρα το ρολόι με τη χρήση του [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol). 
Εισάγετε τη δική σας ζώνη ώρας, και συνεχίστε με την εγκατάσταση.**

![Ρύθμιση ρολογιού](../img/configureclock.png)

&nbsp;
**9) Πριν συνεχίσετε την εγκατάσταση του Devuan χρειάζεται να γίνει κατάτμηση του δίσκο. Αν είναι διαθέσιμη, προτείνεται η επιλογή της χρήσης του μεγαλύτερου διαθέσιμου χώρου. 
Αυτό θα διατηρήσει τις υπάρχουσες κατατμήσεις και δε θα τις αλλάξει. Διαφορετικά μπορείτε να επιλέξετε ολόκληρο το δίσκο αν δεν υπάρχουν δεδομένα που χρειάζεται να σώσετε.

**Εάν χρειάζεστε ολική κρυπτογράφηση δίσκου δείτε [εδώ](full-disk-encryption.md) πριν συνεχίσετε.**

![Χρήση του μεγαλύτερου χώρου](../img/partitiondisks1.png)

&nbsp;

**10) Επιλέγοντας όλα τα αρχεία σε μία κατάτμηση είναι μια λογική επιλογή για τους νεοφερμένους. Η χειροκίνητη κατάτμηση του δίσκους είναι πέρα από τους σκοπούς αυτού του οδηγού.** 

![Όλα τα αρχεία σε μια κατάτμηση](../img/partitiondisks2.png)

&nbsp;

**11) Είναι ώρα να γράψετε αυτές τις κατατμήσεις στο δίσκο και να τις μορφοποιήσετε με το συστήμα αρχείων. Αν είστε ικανοποιημένοι με αυτές τις αλλαγές επιλέξτε εγγραφή στο δίσκο και συνέχεια. 
Θα χρειαστεί να επιβεβαιώσετε πριν γίνουν οι αλλαγές.**

![Εγγραφή αλλαγών στο δίσκο](../img/partitiondisks3.png)

&nbsp;

**12) Τώρα θα γίνει η εγκατάσταση του βασικού συστήματος. Αναλόγως το υλικό σας αυτό μπορεί να πάρει λίγη ώρα.**

![Αναμονή για την εγκατάσταση του βασικού συστήματος](../img/installing.png)

&nbsp;

**13) Ο εγκαταστάτης θα ρωτήσει τώρα εάν θέλετε να χρησιμοποιήσετε ένα δικτυακό καθρέπτη. Εάν έχετε πρόσβαση στο δίκτυο αυτό προτείνεται
επειδή θα έχετε πρόσβαση στις τελευταίες εκδόσεις των πακέτων. Εάν αποφασίσετε να χρησιμοποιήσετε κάποιο δικτυακό καθρέπτη, επιλέξτε τη χώρα σας
από το κατάλογο.**

![Χρήση δικτυακού καθρέπτη](../img/networkmirror1.png)

&nbsp;

**14) Οποιοσδήποτε από τους διαθέσιμους καθρέπτες είναι εντάξει. Αν υπάρχει κάποιος καθρέπτης κοντά σας 
τότε θα ανακατευνθείτε αυτόματα σε αυτόν όταν εγκαθιστάτε πακέτα.**

![Χρήση ενός δικτυακού καθρέπτη](../img/networkmirror3.png)

&nbsp;

**15) Το Devuan μπορεί να χρησιμοποιήσει το διαγωνισμό δημοτικότητας (popcon) για να συλλέγει πληροφορίες σχετικά με τα πιο κοινά πακέτα. 
Αυτό είναι καθαρά προαιρετικό και συλλέγει στατιστικά για τα πακέτα που θα εγκατασταθούν από εκείνη τη στιγμή και μετά.**

![Διαγωνισμός δημοτικότητας](../img/popcon.png)

&nbsp;

**16) Η εγκατάσταση παρέχει κάποια προκαθορισμένα πακέτα που μπορείτε να επιλέξετε. Γενικά οι προεπιλογές είναι μια καλή επιλογή και είναι προτιμότερο να μείνετε σε αυτές.**

![Επιλογή λογισμικού](../img/selectsoftware.png)

&nbsp;

**17) Το πρόγραμμα θα εγκαταστήσει τώρα τα πακέτα που επιλέξατε. Αυτό θα πάρει λίγη ώρα.**

![Εγκατάσταση λογισμικού](../img/retrieving.png)

&nbsp;

**18) Τώρα που όλο το λογισμικό που χρειάζεστε είναι εγκατεστημένο, θα εγκατασταθεί και ο εκκινητής συστήματος GRUB. Αυτό επιτρέπει την εκκίνηση του λειτουργικού συστήματος μετά την εγκατάσταση. 
Αν ερωτηθείτε για να το εγκαταστήσετε στο MBR, τις περισσότερες φορές θα πρέπει να το κάνετε.**

**Κάποιες ρυθμίσεις δε θα χρειαστούν διαμόρφωση και η εγκατάσταση θα τελειώσει τώρα.**

![Εγκατάσταση του εκκινητή συστήματος GRUB](../img/grub-mbr.png)

&nbsp;

**19) Είναι σημαντικό να επιλέξετε τη σωστή τοποθεσία για το πρόγραμμα εκκίνησης. Δε θα πρέπει να εγκατασταθεί σε μια κατάτμηση, αλλά στην περιοχή MBR που βρίσκεται στο σκληρό δίσκο.**
**Σε αυτή τη περίπτωση το `/dev/sda` είναι ο μόνος σκληρός δίσκος οπότε θα το εγκαταστήσουμε εκεί.**

![Επιλογή τοποθεσίας προγράμματος εκκίνησης](../img/grubtohdd.png)

&nbsp;

**20) Η εγκατάσταση έχει ολοκληρωθεί, απομακρύνετε το μέσο εγκατάστασης για να συνεχίσετε με την εκκίνηση στο γραφικό περιβάλλον του Devuan.**

![Ολοκλήρωση εγκατάστασης](../img/finish.png)


---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
