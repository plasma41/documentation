Αυτό το κείμενο δείχνει πως να αναβαθμίσετε το σύστημά σας από τη
Devuan Jessie στη Devuan ASCII. Προϋποθέτει ένα εγκατεστημένο και
λειτουργικό σύστημα Devuan Jessie και δε θα πρέπει να χρησιμοποιηθεί
για μεταναστεύσεις.

# Αναβάθμιση από τη Devuan Jessie στην ASCII
Πρώτα επεξεργαστείτε το αρχείο sources.list ώστε ο κλάδος να αλλάξει
σε ASCII.

`root@devuan:~# editor /etc/apt/sources.list`

Προσθέστε τον τελευταίο καθρέπτη της Devuan και τον κλάδο ASCII όπως
σας δείχνουμε. Απενεργοποιήστε σχολιάζοντας όποια άλλη γραμμή.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Αναβαθμίστε τα κλειδιά της Devuan για να βεβαιωθείτε πως έχετε τη
τελευταία έκδοσή τους.

`root@devuan:~# apt-get upgrade devuan-keyring`

Ανανεώστε τα αρχεία των δεικτών των πακέτων.

`root@devuan:~# apt-get update`

Το μόνο που απομένει είναι η αναβάθμιση του συστήματος.

`root@devuan:~# apt-get dist-upgrade`

---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
