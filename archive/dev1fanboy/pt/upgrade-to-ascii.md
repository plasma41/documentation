Este documento mostra como fazer uma atualização do Devuan Jessie para o Devuan ASCII. Isto assume que você possui uma instância do Devuan Jessie instalada e em funcionamento; e que não deve ser usada para migrações (migrar do Debian para o Devuan).

# Atualize do Devuan Jessie para o ASCII
Primeiro modifique o sources.list para que aponte para as fontes do ASCII.

`root@devuan:~# editor /etc/apt/sources.list`

Adicione os espelhos do Devuan e a ramificação do ASCII conforme mostrado. Comente com uma cerquilha (#) nas linhas que se tornarão obsoletas para que não sejam mais utilizadas.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Atualize o chaveiro do Devuan para se certificar de que possui a versão mais recente.

`root@devuan:~# apt-get upgrade devuan-keyring`

Atualize o índice de pacotes.

`root@devuan:~# apt-get update`

A única coisa que resta agora é aplicar as atualizações ao sistema.

`root@devuan:~# apt-get dist-upgrade`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
