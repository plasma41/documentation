# Devuan sem D-Bus
Este documento descreve como remover D-Bus do Devuan, incluindo migrar para um gerenciador de janelas mais leve, escolher um navegador e uma solução alternativa para a montagem automática do D-Bus.

## Escolhendo um gerenciador de janelas
Ao contrário dos mais completos ambientes de desktop, a maioria dos gerenciadores de janelas não dependem do D-Bus, portanto você deve escolher um deles.

* blackbox
* fluxbox
* fvwm
* fvwm-crystal
* openbox

### Instalando e configurando Fluxbox
Estaremos usando o Fluxbox por ser simples e intuitivo.

`root@devuan:~# apt-get install fluxbox`

Torne o Fluxbox o gerenciador de janelas padrão para seu usuário quando utilizar o script startx.

`user@devuan:~@ echo "exec fluxbox" >> .xinitrc`

Agora você pode invocar o script startx para usar o Fluxbox.

`user@devuan:~@ startx`

Uma boa opção de gerenciador de exibição é o WDM.

`root@devuan:~# apt-get install wdm`

## Escolhendo um navegador
Existem alguns navegadores que dependem de componentes dbus, todavia alguns são melhores que outros. Aqui estão alguns dos navegadores que você pode escolher.

* xombrero
* lynx
* links2
* dillo
* midori
* firefox-esr

Escolheremos o conhecido Firefox ESR, já que possui mais recursos que os demais.

`root@devuan:~# apt-get install firefox-esr`

## Removendo D-Bus do Devuan
Agora podemos remover dbus do Devuan.

`root@devuan:~# apt-get purge dbus`

Devemos também remover quaisquer pacotes órfãos que restaram após a remoção do dbus.

`root@devuan:~# apt-get autoremove --purge`

## Uma alternativa simples para montagem automática
Sem o D-Bus você não terá montagem automática disponível para a maioria dos gerenciadores de arquivos, pois essas partes requerem D-Bus e métodos simples de montagem não são sempre implementados. Nós configuraremos pontos de montagem manualmente para que os gerenciadores de arquivos possam montar volumes com apenas alguns cliques.

### Criando pontos de montagem
Crie uma pasta para o novo ponto de montagem.

`root@devuan:~# mkdir /media/usb0`

Faça backup do seu fstab antes de proceder.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Agora podemos editar o fstab.

`root@devuan:~# editor /etc/fstab`

Precisamos adicionar pontos de montagem para um dispositivo USB ao fim do fstab. Assegure-se de definir a opção `user` para que usuários não root possam montar esse dispositivo.

~~~
/dev/sdb1       /media/usb0     auto    user,noauto     0 0
~~~

Para dispositivos USB, os nós que os identificam variam dependendo da sua configuração. Você pode descobrir quais nós de dispositivo serão usados quando plugar algum deles e rodar o utilitário `lsblk`.

Plugue um dispositivo USB para testar sua configuração.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Escolhendo um gerenciador de arquivos
Para um gerenciador de arquivos gráfico que pode montar e desmontar dispositivos baseando-se em seu fstab, você pode usar o Xfe.

`root@devuan:~# apt-get install xfe`

Um gerenciador de arquivos minimalista e interessante é o Midnight Commander, baseado em ncurses.

`root@devuan:~# apt-get install mc`

---
Tradução por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
