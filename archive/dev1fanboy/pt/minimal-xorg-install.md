# Instalação minimalista do xorg
Este documento descreve como conseguir uma instalação minimalista do xorg com alguns opcionais.

## Instale os pacotes base do xorg
Primeiro faça login em um console como root usando a senha que você configurou durante a instalação.

Instalaremos uma seleção minimalista de pacotes necessários para o xorg.

`root@devuan:~# apt-get install xserver-xorg-video-dummy xserver-xorg-input-void xserver-xorg-core xinit x11-xserver-utils`

Vale a pena mencionar que o driver de entrada (input) 'void' está instalado assim como o driver de vídeo 'dummy'. Esses pacotes não funcionam como drivers, mas permitem que o xorg seja instalado sem os verdadeiros drivers. Isso é feito para manter sua instalação livre de drivers desnecessários. Instalaremos individualmente apenas o que você irá precisar.

## Instale o driver de gráficos
Instalaremos agora o driver de exibição para o seu hardware.

Drivers de exibição para os sistemas mais comuns incluem:

* xserver-xorg-video-intel (intel)
* xserver-xorg-video-nouveau (nvidia)
* xserver-xorg-video-openchrome (via)
* xserver-xorg-video-radeon (amd)
* xserver-xorg-video-vesa (driver de exibição genérico)

Por exemplo, se você possui uma placa gráfica da AMD, deverá instalar o driver radeon.

`root@devuan:~# apt-get install xserver-xorg-video-radeon`

Se não tiver certeza sobre o que instalar, sugerimos que use o vesa por enquanto até que possa aprender mais sobre os drivers do xorg. Irá funcionar com muitos tipos de hardware compatíveis com os padrões VESA, e também é útil como um driver de reserva no caso de haver algum problema de configuração com seu driver padrão.

`root@devuan:~# apt-get install xserver-xorg-video-vesa`

Para placas gráficas não mencionadas acima você pode pesquisar no repositório para encontrar o driver correto para seu hardware.

`root@devuan:~# apt-cache search xserver-xorg-video-.* | pager`

## Instalando seu driver de entrada (input)
Por ser uma dependência do xserver-xorg, não podemos remover o driver evdev. Felizmente, evdev é um driver unificado e é apropriado para a maioria dos dispositivos de entrada. Isso significa que você não precisa seguir esses passos a menos que tenha requisitos específicos.

Se você utiliza mouse e teclado, poderá instalar os drivers de cada um separadamente, se preferir.

`root@devuan:~# apt-get install xserver-xorg-input-mouse xserver-xorg-input-kbd`

Para um touchpad synaptics, você provavelmente terá que instalar o driver synaptics além do driver do teclado.

`root@devuan:~# apt-get install xserver-xorg-input-synaptics`

Se você precisa de drivers para outros tipos de dispositivos de entrada, poderá pesquisá-los no repositório.

`root@devuan:~# apt-cache search xserver-xorg-input-.* | pager`

## Instalando extras opcionais
Recomendamos instalar o pacote básico de fontes para o xorg.

`root@devuan:~# apt-get install xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable`

Você provavelmente irá desejar incluir suporte a mesa opengl se estiver usando os drivers de exibição livres (no sentido de livre expressão), principalmente se for executar jogos opengl.

`root@devuan:~# apt-get install libgl1-mesa-dri mesa-utils`

ASCII pode precisar do pacote xserver-xorg-legacy para gerenciar permissões para o xserver. Note que isso é um wrapper setuid.

`root@devuan:~# apt-get install xserver-xorg-legacy`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
