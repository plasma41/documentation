# Devuan GNU/Linux: instalación, actualización y minimalismo
Esta documentación provee información acerca de la instalación, actualización y
migración a Devuan. También explica cómo reducir el sistema hacia una
instalación más mínima.

## Todas las versiones
[Información general](general-information.md)  
[Instalación de Devuan](devuan-install.md)  
[Cifrado completo del disco](full-disk-encryption.md)  
[Configuración de red](network-configuration.md)  
[Devuan sin D-Bus](devuan-without-dbus.md)  
[Software no dependiente de D-Bus en Devuan](dbus-free-software.md)  
[Instalación mínima de xorg](minimal-xorg-install.md)  
[Instalación mínima de XFCE](minimal-xfce-install.md)  

## ASCII
[Migrar a Devuan ASCII](migrate-to-ascii.md)  
[Actualización desde Devuan Jessie a ASCII](upgrade-to-ascii.md)  

## Beowulf
[Actualizar Devuan a Beowulf](upgrade-to-beowulf.md)  
[Actualización desde Debian Buster](buster-to-beowulf.md)  
[Actualización desde Debian Stretch](stretch-to-beowulf.md)  
[Actualización desde Debian Jessie](jessie-to-beowulf.md)

## Traducciones

Las traducciones están disponibles gracias al equipo de la wiki.

[Read in English](../) **|** [Auf Deutsch lesen](../de) **|** [Διαβάστε στα Ελληνικά](../el) **|** [Leggi in Italiano](../it) **|** [Leia em Português](../pt)

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

