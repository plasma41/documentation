# Cifrado completo del disco

Esta es una guía fácil de seguir para configurar el cifrado completo del disco durante la instalación de Devuan. Dado
que es una guía fácil de seguir, seleccionamos la manera más rápida de hacerlo.

Se necesitará completar los pasos hasta llegar al punto del particionado de disco antes de continuar.

## Particionado del disco

**1) Seleccionar utilizar el disco completo y configurar LVM encriptado. Esta es la forma más simple de hacerlo y es
la recomendada para usuarios nuevos.**

![Particionado con LVM y encripción](../img/crypt1.png)

&nbsp;

**2) Seleccionar el disco que se desee cifrar.**

![Seleccionar el disco a encriptar](../img/crypt2.png)

&nbsp;

**3) Luego será necesario especificar un esquema de particionado como es costumbre. Dado que el objetivo es lograr el
cifrado completo del disco de la manera más simple, seleccionamos todos los archivos en una única partición.**

![Seleccionar un esquema de particionado](../img/10part2.png)

&nbsp;

**4) Es necesario escribir los cambios a disco antes de poder configurar el cifrado.**

![Escribir los cambios a disco](../img/crypt3.png)

&nbsp;

## Cifrado del disco

**5) El instalador informará que está borrando el disco. Este proceso lleva un tiempo si hay mucho espacio en disco
disponible.**

![Borrando el disco de manera segura](../img/crypt4.png)

&nbsp;

**6) Ahora se debe crear una clave de cifrado. Para lograr un buen nivel de seguridad se recomienda utilizar una clave
de 20 caracteres como mínimo, una mezcla de mayúsculas y minúsculas, al menos tres números y dos símbolos.**

![Ingresar la clave de cifrado](../img/crypt5.png)

&nbsp;

**7) Luego es necesario establecer el tamaño del grupo de volúmenes. La forma más simple consiste en dejar el valor por defecto que utilizará todo el disco.**

![Resumen de cambios a ser aplicados](../img/crypt6.png)

**8) Verificar el resumen de cambios, y si está todo correcto aplicarlos a disco.**

![Resumen de cambios a ser aplicados](../img/crypt7.png)

&nbsp;

**9) Finalmente, confirmar los cambios y continuar la [instalación](devuan-install.md#instalar-devuan).**

![Aceptar los cambios](../img/crypt8.png)

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

