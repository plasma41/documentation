# Migrar a Devuan ASCII
Este documento describe el proceso de migración a Devuan ASCII desde Debian Jessie o Stretch. Actualmente la migración a ASCII desde Debian Jessie o Stretch no es directa si se utiliza GNOME o network manager a causa de ciertos paquetes retenidos, y cada migración es ligeramente diferente. Sin embargo este procedimiento debería ser adecuado para resolver los inconvenientes en ambos casos.

## Migración desde GNOME
Debido a que no existe un paquete transicional para GNOME aún, los usuarios de GNOME necesitan instalar xfce4 y slim de manera manual. Seleccionar slim como gestor de pantalla por defecto cuando se solicite.

`root@debian:~# apt-get install xfce4 slim`

Establecer el gestor de sesiones en startxfce4 para que el nuevo escritorio pueda ser utilizado luego.

`root@debian:~# update-alternatives --config x-session-manager`

## Reemplazar network manager con wicd

Para la migración es necesario utilizar wicd en lugar de network manager. Debido
a la falta de una configuración de red compatible, quienes estén migrando desde
Stretch de manera remota **deben** [configurar la red](network-configuration.md)
de forma manual, de lo contrario se perderá el acceso al host remoto durante la
migración.

`root@debian:~# apt-get install wicd`

Asegurarse de que network manager sea eliminado del proceso de inicio.

`root@debian:~# update-rc.d -f network-manager remove`

Las conexiones inalámbricas deberían ahora configurarse para conectarse automáticamente utilizando wicd. Detener network manager antes de configurar la red.

`root@debian:~# /etc/init.d/network-manager stop`

## Ejecutar la migración

Devuan utiliza sysvinit por defecto, por lo tanto es necesario instalarlo.

`root@debian:~# apt-get install sysvinit-core`

Se requiere un reinicio para cambiar sysvinit a PID 1 y eliminar systemd.

`root@debian:~# reboot`

Luego es posible eliminar systemd sin inconvenientes.

`root@debian:~# apt-get purge systemd`

Editar el archivo sources.list para cambiar a los repositorios de Devuan.

`root@debian:~# editor /etc/apt/sources.list`

Agregar los *mirrors* de Devuan con el nombre de rama de ASCII. Comentar el resto de las líneas.

~~~
deb http://deb.devuan.org/merged ascii main
deb http://deb.devuan.org/merged ascii-updates main
deb http://deb.devuan.org/merged ascii-security main
deb http://deb.devuan.org/merged ascii-backports main
~~~

Actualizar los índices de paquetes para poder instalar el archivo *keyring* de Devuan.

`root@debian:~# apt-get update`

Instalar el *keyring* de Devuan así los paquetes pueden ser autenticados desde aquí en adelante.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Actualizar los índices de paquetes nuevamente así se autentican con el *keyring*.

`root@debian:~# apt-get update`

Finalmente es posible migrar a Devuan.

`root@debian:~# apt-get dist-upgrade`

## Tareas post-migración
Los componentes de systemd deberían ahora ser eliminados del sistema.

`root@devuan:~# apt-get purge systemd-shim`

Si no se utiliza D-Bus o xorg es posible eliminar libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Eliminar cualquier paquete huérfano dejado por la instalación previa de Debian.

`root@devuan:~# apt-get autoremove --purge`

Este es un buen momento para limpiar los archivos de paquetes obsoletos dejados por el sistema Debian.

`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

