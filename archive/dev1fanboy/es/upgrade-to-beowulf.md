Este documento describe cómo actualizar a Devuan Beowulf desde un sistema Devuan existente. No debe utilizarse para migraciones desde Debian.
# Actualización a Beowulf

Primero es necesario cambiar el archivo sources.list para que apunte a los repositorios de Beowulf.

`root@devuan:~# editor /etc/apt/sources.list`

Modificar el contenido para que quede como se muestra a continuación. Comentar las líneas restantes.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Actualizar la lista de paquetes desde los repositorios de Beowulf.

`root@devuan:~# apt-get update`

Los usuarios de Devuan Jessie deben actualizar el *keyring* de Devuan para asegurarse de tener la última versión, y actualizar las listas de paquetes nuevamente para que los paquetes sean autenticados.

`root@devuan:~# apt-get install devuan-keyring`  
`root@devuan:~# apt-get update`

Si xscreensaver está en ejecución es necesario matarlo, pues debe detenerse antes de poder actualizarlo.

`root@devuan:~# killall xscreensaver`

Ahora efectuar la actualización.

`root@devuan:~# apt-get dist-upgrade`

En el caso de un fallo con un paquete es necesario corregirlo y reiniciar el proceso de actualización.

`root@devuan:~# apt-get -f install`  
`root@devuan:~# apt-get dist-upgrade`

Los usuarios migrando desde ASCII y que utilizan upower necesitarán hacer un *downgrade* de los paquetes de upower para evitar problemas como: [bug #394](https://bugs.devuan.org/cgi/bugreport.cgi?bug=394).

`root@devuan:~# apt-get install --allow-downgrades upower/beowulf gir1.2-upowerglib/beowulf`

Para finalizar, es deseable eliminar paquetes que hayan quedado huérfanos durante el proceso de actualización junto con archivos de paquetes obsoletos.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

