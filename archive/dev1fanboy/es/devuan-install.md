# Guía de instalación de Devuan

Esta es una guía fácil para instalar Devuan desde una imagen de CD/DVD
para el hardware soportado. El consejo general que acompaña a esta guía es que
se debe generar una copia de respaldo de todos los datos antes de comenzar el
proceso de instalación.

## Contenido
[Prerrequisitos](#prerrequisitos)  
[Arquitecturas soportadas](#arquitecturas-soportadas)  
[Imágenes de instalación](#imágenes-de-instalación)  
[Instalación de Devuan](#instalar-devuan)

## Prerrequisitos

* Los usuarios de sistemas operativos no *nix (Unix, Linux, etc.) deben saber
cómo grabar una imagen en un medio de almacenamiento removible como un
CD/DVD-ROM o un dispositivo USB, cómo cambiar el orden de inicio en el BIOS y
cómo instalar un sistema operativo.
* Otros usuarios deberán saber lo mismo para sistemas *nix y estar algo
familiarizados con el uso de la terminal.

## Arquitecturas soportadas

* amd64
* i386

## Imágenes de instalación

Actualmente estas son las formas de obtener y descargar las imágenes de
instalación de Jessie.

* Directamente desde el [archivo de Devuan](https://files.devuan.org)
* Desde cualquiera de los *mirrors* listados en
[devuan.org](https://www.devuan.org)
* Vía un archivo [torrent](https://files.devuan.org/devuan_beowulf.torrent)
que contiene todas las imágenes disponibles

Si se cuenta con acceso a Internet se sugiere utilizar una única imagen de DVD
completo para la instalación, de lo contrario probablemente se desee utilizar
el conjunto completo.

Por favor utilizar los *mirrors* o torrents cuando sea posible.

Quienes no estén familiarizados con la línea de comandos pueden
[saltar a la instalación](#installing-devuan).

### Verificar la integridad de las imágenes

Antes de grabar la imagen a un medio removible, es mejor verificar la integridad
para asegurarse de que no esté corrompida. Esto evita problemas que puedan
ocurrir luego durante la instalación.

Descargar los `SHA256SUMS` desde el [archivo de Devuan](https://files.devuan.org) y verificar la integridad
de la imagen.

`user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS`

### Verificar las imágenes

Las imágenes de instalación distribuidas por Devuan están firmadas a fin de que
pueda garantizarse que provienen de Devuan. Verificar las imágenes permite
saber que no han sido alteradas por terceros antes de haberlas descargado.

Obtener las [claves de los desarrolladores de
Devuan](https://files.devuan.org/devuan-devs.gpg) e importarlas en nuestro
*keychain*.

`user@hostname:~$ gpg --import devuan-devs.gpg`

Utilizar los `SHA256SUMS` firmados para verificar la imagen.

`user@hostname:~$ gpg --verify SHA256SUMS.asc`

### Grabar la imagen a un CD/DVD o dispositivo USB

Las imágenes pueden ser grabadas en un CD o DVD utilizando wodim.

`user@hostname:~$ wodim dev=/dev/sr0 -eject filename.iso`

Todas las imágenes ISO son híbridas y pueden ser escritas a un dispositivo USB
utilizando dd.

`root@hostname:~# dd if=filename.iso of=/dev/sdX bs=1M && sync`

## Instalar Devuan

Para la instalación se utiliza un instalador no gráfico. Esto es generalmente
más rápido que un instalador gráfico y consume menos recursos. Este
procedimiento es adecuado tanto para instalar en una máquina virtual como para
instalar directamente en hardware. Mientras que puede resultar algo intimidante
para nuevos usuarios, no hay nada que temer dado que el proceso es completamente
guiado.

&nbsp;

**1) Iniciar desde el CD/DVD o dispositivo USB y seleccionar la opción
`Install`. El resto de las opciones están fuera del alcance de esta guía.**

![Menú de instalación de Jessie](../img/01start.png)

&nbsp;

**2) En los siguientes pasos se selecciona el idioma, ubicación y teclado.**

![Distribución de teclado](../img/02keyboard.png)

&nbsp;

**3) El instalador configura la red automáticamente, pero para el caso de redes
inalámbricas el usuario debe indicar el SSID y la contraseña. Luego se debe
elegir un nombre de *host* para el nuevo sistema. No es posible utilizar
espacios o caracteres especiales.**

![Hostname](../img/03hostname.png)

&nbsp;

**4) También se deberá proveer un nombre de dominio. Si no se requiere o se
desconoce, simplemente dejarlo en blanco.**

![Nombre de dominio](../img/04domain.png)

&nbsp;

**5) Se recomienda configurar una clave para el superusuario root. Por razones
de seguridad, se debe elegir una contraseña fuerte. Es necesario repetir el
ingreso de la contraseña para asegurarse de que sea correcta.**

![Contraseña de root](../img/05rootpw.png)

&nbsp;

**6) Luego se debe configurar una cuenta de usuario no privilegiado, que será
utilizado normalmente en la mayoría de los casos. No hay necesidad de ingresar
el nombre completo a menos que haya una razón para ello, por lo cual es posible
proceder e indicar un nombre de usuario.**

![Nombre de usuario](../img/06username.png)

&nbsp;

**7) Al igual que para root, será necesario especificar una contraseña e
ingresarla dos veces.**

![Contraseña del usuario](../img/07userpw.png)

&nbsp;

**8) El instalador configurará ahora el reloj utilizando
[NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol). Ingresar la
información de la zona horaria y continuar la instalación.**

![Configuración del reloj](../img/08ntp.png)

&nbsp;

**9) Antes de poder instalar Devuan el disco debe ser particionado. Si está
disponible como opción, se recomienda elegir el mayor espacio continuo disponible.
En tal caso las particiones existentes no necesitan ser alteradas.
De lo contrario es posible seleccionar el disco completo si no contiene datos
que se deseen mantener. Si se requiere encriptación completa del disco ver este
[enlace](full-disk-encryption.md).**

![Utilizar el espacio más grande disponible](../img/09part1.png)

&nbsp;

**10) Seleccionar todos los archivos en una única partición es lo recomendado
para usuarios nuevos. El particionado manual está fuera del alcance de esta
guía.**

![Todos los archivos en una única partición](../img/10part2.png)

&nbsp;

**11) A continuación se procede a escribir las particiones a disco y formatear
los sistemas de archivos en ellas. Si se está de acuerdo con los cambios,
continuar y escribir el disco. Es necesario confirmar esto antes de que los
cambios se apliquen.**

![Aplicar los cambios en disco](../img/11part3.png)

&nbsp;

**12) Ahora se instala el sistema base. Dependiendo del hardware este proceso
puede llevar cierto tiempo.**

![Instalación del sistema base](../img/12install.png)

&nbsp;

**13) Si no se tiene acceso a Internet o no se desea utilizar un *mirror*, se recomienda escanear CD's adicionales para disponer de mayor cantidad de paquetes a instalar.**

![Escanear CD's](../img/13scancd.png)

**14) El instalador permite seleccionar un *mirror*. Si se posee acceso a la
red esto es lo recomendado debido a que se instalan las últimas versiones
disponibles de los paquetes. En tal caso, seleccionar tu país de la lista.**

![Seleccionar un mirror](../img/14mirror1.png)

&nbsp;

**15) Cualquiera de los *mirrors* seleccionados está bien. En el caso de que
exista un mirror cercano a la ubicación actual, será redireccionado al mismo
de manera automática al momento de instalar los paquetes.**

![Uso de un mirror](../img/15mirror2.png)

&nbsp;

**16) Devuan posee un concurso de popularidad para recolectar información acerca
de los paquetes más utilizados. Esto sólo tiene como propósito mejorar la
calidad de la distribución y sólo recolecta estadísticas de los paquetes
instalados por el instalador y luego el usuario.**

![Concurso de popularidad](../img/16popcon.png)

&nbsp;

**17) La instalación permite elegir paquetes preseleccionados. Generalmente la
selección de paquetes por defecto está bien. En caso de duda mantener la
selección por defecto.**

![Selección del software](../img/17tasksel.png)

&nbsp;

**18) El instalador entonces procede con la instalación de los paquetes
seleccionados. Esto puede demorar un momento.**

![Instalando paquetes](../img/18retrieve.png)

&nbsp;

**19) A continuación se solicita seleccionar un sistema de inicio. Por defecto se utiliza sysvinit.**

![Gestor de inicio](../img/19init.png)


**20) Ahora que todo el software necesario está instalado, el gestor de
inicio GRUB será instalado. Esto permite que el sistema operativo se inicie luego
de la instalación. En caso de que el instalador solicite instalarlo en el sector
MBR, reponder que sí. Algunas instalaciones no requieren configuración y la
instalación finaliza aquí.**

![Instalando el gestor de inicio GRUB](../img/20grub1.png)

&nbsp;

**21) Es importante seleccionar la ubicación correcta para el gestor de inicio.
No debe ser instalado en una partición, sino en el sector MBR del disco rígido.
En este caso `/dev/sda` es el único disco rígido presente, con lo cual se
instala en el mismo.**

![Seleccionar la ubicación del gestor de inicio](../img/21grub2.png)

&nbsp;

**22) La instalación ha terminado, remover el medio de instalación para
continuar e iniciar en el entorno gráfico de Devuan.**

![Finalizada la instalación](../img/22finish.png)

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0
Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)].
Todas las marcas registradas son propiedad de sus respectivos dueños. Este
trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>
