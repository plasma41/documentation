Estas instrucciones son para migrar exclusivamente desde Debian Jessie. Cuando se migra a Beowulf las instrucciones son específicas para cada versión de Debian desde la que se proviene y deben seguirse al pie de la letra.

# Migrar desde Debian Jessie a Beowulf

Es necesario [configurar la red](network-configuration.md) antes de empezar, de lo contrario se perderá el acceso a la red durante la migración.

La forma más simple y fácil de comenzar la migración consiste en instalar primero sysvinit-core.

`root@debian:~# apt-get install sysvinit-core`

Luego reiniciar para que sysvinit sea PID 1.

`root@debian:~# reboot`

Los usuarios de Gnome deberá remover la herramienta tweak para evitar el bloqueo de paquetes. Esto puede tener efectos en el escritorio, pero luego de la migración se podrán instalar los paquetes de Gnome provistos por Devuan si se desea.

`root@debian:~# apt-get purge gnome-tweak-tool`

Ahora cambiar a los repositorios de Devuan Beowulf.

`root@debian:~# editor /etc/apt/sources.list`

Modificar el contenido para que quede como se muestra a continuación. Comentar las líneas restantes.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Actualizar la lista de paquetes desde los repositorios de Beowulf.

`root@debian:~# apt-get update`

Instalar el *keyring* de Devuan para que los paquetes puedan ser autenticados.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Actualizar las listas de paquetes nuevamente para que los paquetes sean autenticados de ahora en adelante.

`root@debian:~# apt-get update`

Si se opta por utilizar wicd durante la configuración de red, será necesario ejecutar un upgrade normal contra los repositorios de Beowulf antes de continuar.

`root@debian~# apt-get upgrade`

Ahora es posible ejecutar la migración adecuadamente.

`root@debian:~# apt-get dist-upgrade`

Si algo se rompe será necesario arreglarlo y correr dist-upgrade nuevamente.

`root@debian:~# apt-get -f install`  
`root@debian:~# apt-get dist-upgrade`

Ahora es posible eliminar systemd.

`root@debian:~# apt-get purge systemd`

Quienes deseen utilizar Gnome deben asegurarse de instalar el paquete task-gnome-desktop ahora.

`root@devuan:~# apt-get install task-gnome-desktop`

O utilizar el entorno por defecto de Devuan: XFCE.

`root@devuan~# apt-get install task-xfce-desktop`

La migración seguramente dejará algunos paquetes huérfanos y archivos obsoletos. Para obtener un sistema más minimalista, es recomendable eliminarlos.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

