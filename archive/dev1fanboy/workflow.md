## Purpose of the workflow

The main purpose of the new workflow is to lower the amount of work needed in maintaining documents and translations, the end goal being to push complete and finalized translations to the website. This is a draft document and will probably stay that way for a while, to add flexibility to the process. This is a 5-step workflow based on reflections since running this wiki ('dev1fanboy' under devuan-doc, formerly upgrade-install-devuan).

## Workflow steps

1. Develop

This part of the workflow cycle should not start before the last workflow
cycle is complete. If this condition is met:

1a) This part of the workflow applies only to English docs.

1b) Documentation for a new release may start.

1c) Major improvements to docs can be made.

1d) Docs that track the most recent release should be updated.

1e) New docs can be added to the English pages but they should 
consider the default and standard ways of doing things.

1f) Docs for releases very close to deprecated should be archived, to ensure the website is not out of date.

2. Review

2a) Review is for new documents to avoid mistakes and add improvements 
before they are frozen.

2b) All development docs should be reviewed before being frozen.

3. Freeze

Purpose of the freeze is to try to eliminate the need to update translations.

3a) Freeze should happen automatically for previous releases when a 
new development cycle starts. This allows translation to be finalized on
previous releases and gives translators something to do.

3b) Development docs should be soft-frozen until a release is being prepared.

3c) All development docs must be frozen before they can be translated.

4. Translate

4a) Only frozen docs should be translated.

4b) English placeholders should be in place ready for translation.

4c) Add translated pages to front page as they are finished.

4d) All links should use relative paths instead of fixed paths for internal content.

4e) A single integrated branch is used for all translations in devuan-doc.

5. Publish

Purpose of this step is to get finalized docs pushed to the iso's then the website.

5a) Docs for the iso should be converted or updated after the freeze.

5b) Prepare 'dev1fanboy' space to be pushed to devuan-doc after the freeze, including any completed translations.

5c) Files from devuan-doc should be pushed to 'dev1fanboy' on the website if 5a is complete.
