# The path towards devuan 1.0

 * build a [debian-installer](https://git.devuan.org/devuan-packages/debian-installer) based ISO modified with our packages (@nextime, help is welcome)
 * build [base packages](https://git.devuan.org/search?utf8=✓&search=base&group_id=593) for devuan modification on top of jessie ([devuan-baseconf](https://git.devuan.org/devuan-packages/devuan-baseconf), corefiles and [lsb](https://git.devuan.org/devuan-packages/lsb)) (@nextime, help is welcome)
 * rebuild all packages that can be recompiled without systemd dependency ( help needed! )
 * party!

## The planet

[10464 Jessie](http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=10464) was discovered on September 17, 1979 at the Agassiz Station of the Harvard College Observatory.  It 
was named after Jessica Lynn "Jessie" Peterson (1994-2009).

[releases](../releases) - previous: none - this release: [jessie](jessie-1.0) - next: [ascii](ascii-2.0) - unstable: [ceres](ceres)

