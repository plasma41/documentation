# Running Devuan OS in a virtual machine with qemu

UPDATES:
- Moved the original page to [try-devuan-alpha-on-qemu](try-devuan-alpha-on-qemu)
- Scroll down for a tip on [sharing folders](#sharing-a-folder-with-the-host)

To run Devuan with QEMU, you will need 5GB of free disk space, and `qemu-kvm`.  On Debian, install it with `apt-get install qemu-kvm`.

## Get the goodies

```
wget https://files.devuan.org/devuan_jessie_beta/SHA256SUMS
wget https://files.devuan.org/devuan_jessie_beta/SHA256SUMS.asc
wget https://files.devuan.org/devuan_jessie_beta/devuan_jessie_1.0.0-beta_amd64_CD.iso
```

## Verify the ISO integrity

First, verify that the SHA256SUMS file is genuine.  The GnuPG key used to sign the ISO is: `0x73B35DA54ACB7D10`.

```
gpg --recv-keys 0x73B35DA54ACB7D10 # that's jaromil's key
gpg --verify SHA256SUMS.asc
```
Should say: `Good signature from "Denis Roio (Jaromil) <jaromil@dyne.org>"`

Then compare the sums:

```
grep devuan_jessie_1.0.0-beta_amd64_CD.iso SHA256SUMS
sha256sum devuan_jessie_1.0.0-beta_amd64_CD.iso
```
Should say: `2fa1bf7d9b9ca3a948f9a658fe64d56f1679f4aa0bcd85c8a09ce4a3cbf4bc38`.

## Create a virtual disk

```
qemu-img create devuan_disk 5G
```

## Boot the ISO

```
# First run: boot from the ISO
qemu-system-x86_64 -cdrom devuan_jessie_1.0.0-beta_amd64_CD.iso \
    -hda devuan_disk -boot d -net nic -net user -m 256 -localtime
```

## Boot the installed system

```
qemu-system-x86_64 -hda devuan_disk -boot c -net nic -net user -m 256 -localtime
```

Tadaa!  Enjoy datalove <3!

If you want speed, add the `-enable-kvm` option.  You can also give it more RAM, connect with SSH (for local keyboard, until changed in X), and use `VNC`.  Replace this last step:

```
qemu-system-x86_64 -enable-kvm -hda devuan_disk -boot c -net nic \
    -net user,hostfwd=tcp::5556-:22 -m 768 -vnc 127.0.0.1:0 -localtime
```

### Sharing a folder with the host

On the host:
```
mkdir ~/STUFF
touch ~/STUFF/hello_from_host
qemu-system-x86_64 -hda devuan_disk -boot c -net nic -net user -m 256 -localtime -fsdev local,id=STUFF,path=~/STUFF,security_model=none -device virtio-9p-pci,fsdev=STUFF,mount_tag=STUFF
```

On the guest:
```
# mkdir /home/STUFF
# mount -t 9p STUFF /home/STUFF
# ls /home/STUFF
hello_from_host
```

### Login

with `SSH`:
```
ssh login: ssh -p 5556 localhost
```

Or with `VNC`:
```
vnc display: vncviewer localhost:0
```

# Bugs?  Who need'em?

Please [report any issue](../issues/new?issue[milestone_id]=4) to the [Jessie 1.0.0-beta2](../issues?milestone_id=4) milestone.
