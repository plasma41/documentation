# Running Devuan OS in a virtual machine with qemu

**This page is kept for historical reasons.  Please refer to [the up-to-date version](try-devuan-on-qemu)**.


UPDATES:
 -  Nightly build images are now hosted on https://files.devuan.org/ **STILL NOT FOR PRODUCTION**
 -  Nightly build images of the alpha release can be found at ~~https://files.dyne.org/devuan/~~
    **USE THEM AT YOUR OWN RISK!**
 -  Scroll down for a tip on [sharing folders](#sharing-a-folder-with-the-host)

# Happy Datalove Day!

@jaromil decided to make a __[pre-alpha Valentine day release](http://mirror.debianfork.org/devuan-jessie-i386-xfce-prealpha-valentine.iso)__ to show his `<3` love to @nextime and @KatolaZ came up with this really simple howto for you to test. ([Read the announcement](https://lists.dyne.org/lurker/message/20150214.213545.7ba2d477.en.html))

You will need 5GB of free disk space, and `qemu-kvm`.  On Debian, install it with `apt-get install qemu-kvm`.

## Get the goodies

```
wget http://mirror.debianfork.org/devuan-jessie-i386-xfce-prealpha-valentine.iso{,.asc,.sha}
```

## Verify the ISO integrity

```
cat devuan-jessie-i386-xfce-prealpha-valentine.iso.sha
sha256sum devuan-jessie-i386-xfce-prealpha-valentine.iso
```
Should say: `b7f1423441be5a2694f38e489bbfd825b9f19f7cac806090f93655250ba0bf05`

The GnuPG key used to sign the ISO is: `0x73B35DA54ACB7D10`.

```
gpg --recv-keys 0x73B35DA54ACB7D10
gpg --verify devuan-jessie-i386-xfce-prealpha-valentine.iso.asc
```
Should say: `Good signature from "Denis Roio (Jaromil) <jaromil@dyne.org>"`

## Create a virtual disk

```
qemu-img create devuan_disk 5G
```

## Boot the ISO

```
# First run: boot from the ISO
qemu-system-x86_64 -cdrom devuan-jessie-i386-xfce-prealpha-valentine.iso \
    -hda devuan_disk -boot d -net nic -net user -m 256 -localtime
```

## Boot the installed system

```
qemu-system-x86_64 -hda devuan_disk -boot c -net nic -net user -m 256 -localtime
```

Tadaa!  Happy datalove day!

If you want speed, add the `-enable-kvm` option.  You can also give it more RAM, connect with SSH (for local keyboard, until changed in X), and use `VNC`.  Replace this last step:

```
qemu-system-x86_64 -enable-kvm -hda devuan_disk -boot c -net nic \
    -net user,hostfwd=tcp::5556-:22 -m 768 -vnc 127.0.0.1:0 -localtime
```

### Sharing a folder with the host

On the host:
```
mkdir ~/STUFF
touch ~/STUFF/hello_from_host
qemu-system-x86_64 -hda devuan-ceres-amd64.qcow2 -boot c -net nic -net user -m 256 -localtime -fsdev local,id=STUFF,path=~/STUFF,security_model=none -device virtio-9p-pci,fsdev=STUFF,mount_tag=STUFF
```

On the guest:
```
# mkdir /home/STUFF
# mount -t 9p STUFF /home/STUFF
# ls /home/STUFF
hello_from_host
```

### Login

with `SSH`:
```
ssh login: ssh -p 5556 localhost
```

Or with `VNC`:
```
vnc display: vncviewer localhost:0
```

# Bugs?  Who need'em?

Please [report any issue](../issues/new?issue[milestone_id]=3) to the [Datalove Pre-Release](../issues?milestone_id=3) milestone.
