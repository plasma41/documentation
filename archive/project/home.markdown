Devuan Project
==============

Welcome to the [Devuan project](http://www.devuan.org), a fork of
[debian](http://www.debian.org) (starting at Jessie). Devuan aims at providing:
+ a lighter project organization, in particular without the political bloats that has grown around Debian
+ a distro that fosters freedom of choice for users and sysadmins,
+ a distro that avoids any lock-in in the most important parts of the system

The latter two points mean that we are trying to avoid anything that will hijack the GNU part of the system, like systemd is doing right now in many distros and also in our lovely Debian.

We consider ourselves the actual Debian legacy.  We believe Devuan should remain simple and anything that Debian 8 imposed in the base install should belong to derivative works, either _blends_, _seeds_, or _derivative distros_, simply because __as a universal system__, Devuan must encourage innovation, experimentation, and invention, and not corner developers into using a specific approach.

This wiki offers guidelines for contributors and developers, with the hope to get enough traction in the community to keep up with the amazing tradition of Debian, POSIX and the UNIX philosophy that forms its foundation.

## Meta

* [Project description and primary goals](ProjectDescription)
* [Decisional structure](DecisionalStructure)
* [Constitution](DevuanConstitution)
* [Infrastructure](InfraStructure)

## [Releases](releases)

* [Devuan codenames](devuan-codenames)
* [Devuan suites](releases):
 * **Stable: [Devuan 1.0 Jessie](releases/jessie)**
 * Testing: [ASCII](releases/ascii)
 * Unstable: [Ceres](releases/ceres)

