Devuan aims to be a stable, production ready fork of Debian Jessie, free from the entangling web of dependencies imposed by systemd.

This is just the start of a process, as bold as it sounds to call it a fork of Debian. This exodus is ultimately being a relief for us, leading to the creation a peaceful space for work we are well able to do. Believe it or not, there are many users and ICT professionals needing to opt-out from systemd and if you kept reading until here you probably are one. We are not alone!

Devuan aims to be a collective effort for a base distribution to suit our immediate needs, abiding to the long-term mission of protecting the freedom of its community of users and developers. Our priority is to enable diversity, interoperability and backward compatibility for upstream developers and downstream distributions willing to preserve their freedom.
Devuan is made necessary as we believe Debian has strayed from this mandate.

For the 1.0 release Devuan derives its own installer and package repositories from Debian Jessie, applying the necessary modifications to remove systemd. Our objective for 2015 is to make anyone using Debian Wheezy or Jessie able to update or switch to Devuan 1.0.

Devuan rebuilds an infrastructure similar to Debian, while taking the opportunity to innovate some of its practices.

Devuan developers look at this project as a fresh new start for a community of interested people and do not intend to vexate any participant with hierarchy and bureaucracy beyond real cases of emergency. We are well conscious this is possible for us mostly because of starting small again; we will do our best to not repeat the same mistakes and we welcome all Debian Developers willing to join us on this route.

As Devuan developers, we make an effort to improve the relationship with both upstream and downstream and, particularly in this gestational phase we do our best to accomodate needs of those downstream distributions willing to adopt Devuan as base.

On the long-term Devuan will do its best to stay minimal and abide to the UNIX philosophy of "doing one thing and doing it well". Devuan perceives itself not as an end product, but a starting point for developers, a viable base for sysadmins and a stable tool for people who have experience of Debian. As such, Devuan will actively support and maintain the subset packages that require work to ensure the init freedom and freedom of choice in other subsystems, whilst still allowing use of the huge range of packages that are inherited from Debian otherwise untouched. Devuan will never impose choices to gain more efficiency at the cost of its users' freedom, rather than leave such concerns to the independent choices made by downstream developers.